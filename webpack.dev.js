const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const DashboardPlugin = require('webpack-dashboard/plugin');

// Multiple CSS output
const extractApp = new ExtractTextPlugin('app.css');
const extractEditorStyle = new ExtractTextPlugin('editor-style.css');

module.exports = {
	devtool: 'inline-source-map',
	
	entry: [
		'./src/js/main.js',
		'./src/sass/app.scss',
		'./src/sass/editor-style.scss'
	],

	plugins: [
		extractApp,
		extractEditorStyle,
		new DashboardPlugin()
	],

	output: {
		path: __dirname,
		filename: 'includes/js/scripts.js'
	},

	externals: {
		jquery: 'jQuery',
		underscore: '_',
		backbone: 'Backbone',
		wp: 'wp'
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ["babel-loader"]
			},
			{
				test: /editor-style\.scss$/,
				use: extractEditorStyle.extract({
					fallback: "style-loader",
					use: [
						{ 
							loader: "css-loader",
							options: {
								// sourceMap: 'true'
							}
						},
						{ loader: "resolve-url-loader" },
						{
							loader: "sass-loader",
							options: {
								outFile: 'editor-style.css',
								outputStyle: 'expanded'
								// sourceMap: 'true'
							}
						}
					]
				})
			},
			{
				test: /app\.scss$/,
				use: extractApp.extract({
					fallback: "style-loader",
					use: [
						{ 
							loader: "css-loader",
							options: {
								sourceMap: true
							}
						},
						{ loader: "resolve-url-loader" },
						{ loader: "postcss-loader", options: { sourceMap: true } },
						{
							loader: "sass-loader",
							options: {
								outFile: 'app.css',
								outputStyle: 'expanded',
								sourceMap: true
							}
						}
					]
				})
			},
			{
				test: /\.(png|jpg|jpeg)(\?[0-9].+)?$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'file-loader',
						options: {
							limit: '10000',
							name: "images/[name].[ext]"
						}
					}
				]
			},
			{
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				use: [
					{
						loader: "file-loader",
						options: {
							limit: '10000',
							mimetype: "application/font-woff",
							name: "fonts/[name].[ext]"
						}
					}
				]
			},
			{
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "fonts/[name].[ext]"
						}
					}
				]
			},
			{
				test: /(default-skin|preloader)\.(png|svg|gif)(\?[0-9].+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							limit: '10000',
							name: "images/[name].[ext]"
						}
					}
				]
			}
		]
	},

	stats: {
		// Colored output
		colors: true
	}
};
