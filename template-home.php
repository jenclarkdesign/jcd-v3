<?php
/**
 * Template Name: Home
 */
get_header(); ?>

<?php
    get_template_part( 'partials/sections/hero-section' );
    get_template_part( 'partials/sections/project-section' );
    get_template_part('partials/sections/ongoing-clients-section');
    get_template_part( 'partials/sections/services-section' );
    get_template_part( 'partials/sections/testimonial-section' );
    get_template_part( 'partials/sections/instagram-section' );
?>

<?php get_footer();?>
