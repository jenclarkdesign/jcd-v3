<?php
/**
 * Custom Functions
 *
 * @package WordPress
 * @since 1.0
 */

/**
 * This theme uses Featured Images (also known as post thumbnails)
 * for per-post/per-page Custom Header images
 */
add_theme_support( 'post-thumbnails' );
add_theme_support( 'title-tag' );


/**
 * Register Thumbnail Size
 */
add_image_size( 'hero-image', 1400 );
add_image_size( 'about-image', 648, 467, true );
add_image_size( 'project-image', 600, 600, true );
add_image_size( 'project-gallery-image', 1000 );
// add_image_size( 'service-image' ,500, 500, true );
// add_image_size( 'instagram-image', 160, 160, true );
add_image_size( 'about-thumb', 400 );
add_image_size( 'member-thumbs', 120, 60, true );
add_image_size( 'blog-thumb', 519, 326, true );

//add_image_size( 'page-heading-image', 1280, 300, true );

/**
 * Add Editor Styles
 */
function jcd_add_editor_styles() {
    // $font_url = str_replace( ',', '%2C', '//fonts.googleapis.com/css?family=Titillium+Web:400,700' );
    add_editor_style( array(
        'editor-style.css',
        // $font_url,
    ));
}
add_action( 'init', 'jcd_add_editor_styles' );


/**
 * Register Nav Menus
 */
add_theme_support( 'nav-menus' );
register_nav_menus( array(
    'main-menu' => __( 'Main Menu', 'jcd' ),
    'footer-menu' => __( 'Footer Menu', 'jcd' ),
    
) );


/**
 * Print Author post Link
 */
function jcd_author_links( $id = '' ) {
    if( $id ) : ?>
        <a href="<?php echo esc_url( get_author_posts_url( $id ) ); ?>" rel="author" class="author">
            <span class="fn"><?php echo get_the_author_meta( 'user_nicename', $id ); ?></span>
        </a>
    <?php endif;
}


/**
 * Social Links
 */
function jcd_social_links() {
    global $post;
    $permalink = get_permalink( $post->ID );
    ?>

    <div class="entry-social social-links">
      <a href="#" data-url="<?php echo $permalink; ?>" data-text="<?php echo get_the_title( $post->ID ); ?>" class="facebook"><i class="icon-facebook"></i></a>
      <a href="#" data-url="<?php echo $permalink; ?>" data-text="<?php echo get_the_title( $post->ID ); ?>" class="twitter"><i class="icon-twitter"></i></a>
      <a href="#" data-url="<?php echo $permalink; ?>" class="googleplus"><i class="icon-google-plus"></i></a>
      <a href="#" data-url="<?php echo $permalink; ?>" class="linkedin"><i class="icon-linkedin"></i></a>
      <!-- <a href="#" data-url="<?php echo $permalink; ?>" class="blogger"><i class="icon-blogger"></i></a> -->
      <!-- <a href="#" data-url="<?php echo $permalink; ?>" class="wordpress"><i class="icon-wordpress"></i></a> -->
      <a target="_blank" href="http://www.tumblr.com/share/link?url=<?php echo urlencode($permalink) ?>&name=<?php echo urlencode(get_the_title( $post->ID )) ?>&description=<?php echo urlencode($post->post_excerpt); ?>" class="tumblr"><i class="icon-tumblr"></i></a>
    </div>

    <?php
}


/**
 * Get oEmbed meta
 */
function get_first_oembed( $id ) {
    $meta = get_post_custom($id);

    foreach ($meta as $key => $value) {
        if (false !== strpos($key, 'oembed')) {
            if( false !== strpos($value[0], 'iframe') ) {
                return $value[0];
            }
        }
    }
}


/**
 * Get Copyrights
 */
function jcd_copyrights() {
    if( get_option( 'jcd_copyrights_text' ) ) {
        echo get_option( 'jcd_copyrights_text' );
    } else {
        echo ' &copy; Copyright Jen Clark Design, Melbourne, Australia '.date('Y').'.';
    }
}


/**
 * Remove [..] in excerpt
 */
function jcd_trim_excerpt($text) {
    // return rtrim($text,'[&hellip;]').'&hellip;';
    return '&hellip;';
}
add_filter('excerpt_more', 'jcd_trim_excerpt');


/**
 * Add Extra Upload Mime Types
 */
add_filter( 'upload_mimes', 'jcd_add_custom_upload_mimes' );
function jcd_add_custom_upload_mimes( $mimes ) {
    $mimes = array_merge( $mimes, array(
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'xlsx|xls' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'mp3' => 'audio/mpeg',
        'svg' => 'image/svg+xml',
    ) );

    return $mimes;
}


/**
 * Helper for checking if paged called from ajax
 */
function is_doing_ajax() {
    return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}


if ( ! function_exists( 'jcd_comment_nav' ) ) :
/**
 * Display navigation to next/previous comments when applicable.
 *
 * @since Twenty Fifteen 1.0
 */
function jcd_comment_nav() {
    // Are there comments to navigate through?
    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
    ?>
    <nav class="navigation comment-navigation" role="navigation">
        <h2 class="screen-reader-text"><?php _e( 'Comment navigation', 'twentyfifteen' ); ?></h2>
        <div class="nav-links">
            <?php
                if ( $prev_link = get_previous_comments_link( __( 'Older Comments', 'twentyfifteen' ) ) ) :
                    printf( '<div class="nav-previous">%s</div>', $prev_link );
                endif;

                if ( $next_link = get_next_comments_link( __( 'Newer Comments', 'twentyfifteen' ) ) ) :
                    printf( '<div class="nav-next">%s</div>', $next_link );
                endif;
            ?>
        </div><!-- .nav-links -->
    </nav><!-- .comment-navigation -->
    <?php
    endif;
}
endif;


/**
 * Add Class to Contact Form 7 Form
 */
add_filter( 'wpcf7_form_class_attr', 'jcd_wpcf7_classname' );
function jcd_wpcf7_classname( $class ) {
    $class .= ' contact-form';
    return $class;
}


/**
 * Change excerpt more
 */
function jcd_excerpt_more( $more ) {
    return;
    //return ' <a href="'. get_permalink( get_the_ID() ) .'" class="readmore-link" data-id="'. get_the_ID() .'">More &rsaquo;</a>';
}
add_filter('excerpt_more', 'jcd_excerpt_more');


/**
 * Change excerpt length
 */
function jcd_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', 'jcd_excerpt_length', 999 );


/**
 * Get Share URL
 */
function get_share_url( $type, $url, $title = '' ) {
    if ( 'facebook' == $type ) {
        return add_query_arg( array(
            'u' => urlencode( $url ),
        ), 'https://www.facebook.com/sharer/sharer.php' );
    }

    elseif ( 'twitter' == $type ) {
        return add_query_arg( array(
            'text' => $title,
            'url' => $url,
        ), 'https://twitter.com/intent/tweet' );
    }

    elseif ( 'linkedin' == $type ) {
        return add_query_arg( array(
            'mini' => 'true',
            'url' => urlencode( $url ),
            'title' => $title,
            'source' => 'LinkedIn'
        ), 'https://www.linkedin.com/shareArticle' );
    }

    elseif ( 'googleplus' == $type ) {
        return add_query_arg( array(
            'url' => urlencode( $url ),
        ), 'https://plus.google.com/share' );
    }
}


/**
 * Add Highlight Shortcode
 */
add_shortcode( 'highlight', 'jcd_highlight_shortcode' );
function jcd_highlight_shortcode( $atts, $content = "" ) {
    return '<div class="high">'. $content .'</div>';
}

/**
 * Add Custom Style for TinyMce format dropdown
 */
function jcd_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter('mce_buttons_2', 'jcd_mce_buttons_2');

function jcd_mce_before_init_insert_formats( $init_array ) {
    // Define the style_formats array
    $style_formats = array(
        array(
            'title'   => 'Highlight',
            'block'   => 'span',
            'classes' => 'text-highlight',
            'wrapper' => true,
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}
add_filter( 'tiny_mce_before_init', 'jcd_mce_before_init_insert_formats' );

/**
 * Show Video Embed
 */
function jcd_show_video_embed( $url ) {
    // Check if facebook
    if ( strpos( $url, 'facebook' ) !== false ) {
        ob_start(); ?>

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=903834219673337";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <div class="fb-video" data-href="<?php echo $url; ?>">
            <div class="fb-xfbml-parse-ignore"></div>
        </div>

        <?php
        $content = ob_get_clean();
        return $content;

    } else {
        return wp_oembed_get( $url );
    }
}


/**
 * Facebook Embed Shortcode
 */
function facebook_embed_shortcode( $atts ) {
    if( isset( $atts['url'] ) ) {
        return jcd_show_video_embed( $atts['url'] );
    }
}
add_shortcode( 'fb-video', 'facebook_embed_shortcode' );


/**
 * Modify the Breadcrumbs
 */
add_filter( 'jcd_breadcrumbs_trail', 'jcd_add_breadcrumb_trails', 10, 2 );
function jcd_add_breadcrumb_trails( $trails, $args ) {
    global $post;
    unset( $trails['trail_end'] );
    if(is_page()){

        $trails[] = '<a href="'. get_permalink() .'">'. get_the_title() .'</a>';
    }else if(is_home()){
        $blog_id = get_option( 'page_for_posts' );
        $trails[] = '<a href="'. get_permalink($blog_id) .'">'. get_the_title($blog_id) .'</a>';

    }else{
        $category = get_the_category( $post );
        if( !is_wp_error( $category ) && count( $category ) > 0 ) {
            $trails[] = '<a href="'. get_category_link( $category[0] ) .'">'. $category[0]->name .'</a>';
        }
    }
    return $trails;
}

/**
 * get all projects
 */
function get_all_projects() {
    $args = array(
        'post_type'      => 'projects',
        'post_status'    => array( 'publish' ),
        'posts_per_page' => -1,
    );

    $return = new WP_Query( $args );

    return $return; 
}

/**
 * include counter visitor for post
 *
 **/
add_action( 'template_redirect', 'jcd_count_post_visitor' );
function jcd_count_post_visitor() {
    if( is_singular() ) {
        global $post;
        $_visitor_count = get_post_meta( $post->ID, '_visitor_count', true );
        $_visitor_count = $_visitor_count ? $_visitor_count : 0;
        update_post_meta( $post->ID, '_visitor_count', $_visitor_count + 1 );
    }
}


/**
 * Get Attachment ID from URL
 */
function jcd_get_attachment_id_from_url( $attachment_url = '' ) {
    global $wpdb;
    $attachment_id = false;

    // If there is no url, return.
    if ( '' == $attachment_url )
        return;

    // Get the upload directory paths
    $upload_dir_paths = wp_upload_dir();

    // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
    if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

        // If this is the URL of an auto-generated thumbnail, get the URL of the original image
        $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

        // Remove the upload path base directory from the attachment URL
        $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

        // Finally, run a custom database query to get the attachment ID from the modified attachment URL
        $attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );

    }

    return $attachment_id;
}


/**
 * Get First Image from the Post
 */
function jcd_get_post_first_image( $post_id ) {
    if ( ! $post_id ) {
        return false;
    }

    $post = get_post( $post_id );
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);

    if ( isset( $matches[1][0] ) ) {
        // Remove the query string from URL
        $first_image = $matches[1][0];
        $first_image = preg_replace( '/\?.*/', '', $first_image );

        $image_id = jcd_get_attachment_id_from_url( $first_image );
        
        if ( $image_id ) {
            return $image_id;
        } else {
            return false;
        }
    } else {
        return false;
    }
}


/**
 * Set the posts_per_page for archive project
 */
add_action( 'pre_get_posts', 'jcd_project_show_all' );
function jcd_project_show_all( $q ) {
    if ( is_post_type_archive( 'projects' ) ) {
        $q->set('posts_per_page', -1);
    }
}

// /**
//  * Filter the body class, to show that ATF is exists in the page
//  */
// add_filter( 'body_class', 'jcd_atf_body_class' );
// function jcd_atf_body_class( $classes ) {
// 	global $post;
// 	$is_atf_active = false;
    
// 	if ( is_page_template( 'template-home.php' ) ) {
// 		$custom_content = get_field('content_builder' );

// 		foreach ( $custom_content as $index => $data ) {
// 			// If there is an ATF section in the first row
// 			if ( $data['acf_fc_layout'] === 'atf_section' && $index === 0 ) {
// 				$is_atf_active = true;
// 				continue;
// 			}
// 		}

// 		if ( $is_atf_active ) {
// 			$classes[] = 'atf-active';
// 		}
// 	}

// 	return $classes;
// }


// /**
//  * project id retrieval by category
//  */
// function get_project_by_categories( $term_id, $count ) {
//     $args = array( 
//         'post_type'      => 'projects',
//         'post_status'    => array( 'publish' ),
//         'posts_per_page' => $count,
//         'fields'         => 'ids',
//         'tax_query'      => array(
//             array(
//                 'taxonomy' => 'categories-project',
//                 'field'    => 'term_id',
//                 'terms'    => array( $term_id ),
//             ),
//         ),
//     );

//     $query = get_posts( $args );
//     return $query;
// }


// /**
//  * the addition of project id if the project id is still less than the amount specified
//  */
// function get_project_by_categories_addition( $project_not_in, $count ) {
//     $args = array( 
//         'post_type'      => 'projects',
//         'post_status'    => array( 'publish' ),
//         'posts_per_page' => $count,
//         'post__not_in'   => $project_not_in,
//         'orderby'        => 'rand',
//     );

//     $query = get_posts( $args );
// 	return $query;
// }
