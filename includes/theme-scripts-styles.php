<?php
/**
 * Hook into enqueue_scripts to enqueue scripts and styles
 *
 * @package WordPress
 * @since 1.0
 */
if( !function_exists( 'jcd_scripts_styles' ) ) {
    function jcd_scripts_styles() {
        $is_debug = WP_DEBUG;

        if (isset($_GET['debug'])) {
            $is_debug = true;
        }

        $debug = $is_debug ? '' : '.min';

        // Dequeue media element style
        wp_deregister_style('wp-mediaelement');

        // Include main style
        wp_enqueue_style('jcd-app', get_stylesheet_directory_uri() . '/app' . $debug . '.css');
        wp_enqueue_style('jcd-main', get_stylesheet_uri());

        // Modernizr
        wp_enqueue_script('modernizr', get_template_directory_uri() . '/includes/js/modernizr.js', array(), '3.5.0', false);
		// wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false' );

        // Include the javascript
        wp_enqueue_script('jquery');
        wp_enqueue_script('jcd-scripts', get_template_directory_uri() . '/includes/js/scripts' . $debug . '.js', array('jquery'), '1.0', true);

        wp_localize_script( 'jcd-scripts', 'jcd_config', array(
            'ajaxurl' => admin_url( 'admin-ajax.php' ),
            'homeurl' => home_url('/'),
        ) );


    }
}
add_action( 'wp_enqueue_scripts', 'jcd_scripts_styles' );
