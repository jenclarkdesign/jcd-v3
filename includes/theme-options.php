<?php
if (!function_exists( 'jcd_options')) {
    function jcd_options() {

    // THEME VARIABLES
    $themename = "Jen Clark Design";
    $themeslug = "jcd";

    // STANDARD VARIABLES. DO NOT TOUCH!
    $shortname = "jcd";

    //Access the WordPress Categories via an Array
    $jcd_categories = array();
    $jcd_categories_obj = get_categories( 'hide_empty=1' );

    foreach ($jcd_categories_obj as $jcd_cat) {
        $jcd_categories[$jcd_cat->term_id] = $jcd_cat->name;}
            // $jcd_categories2 = $jcd_categories;
            // $categories_tmp = array_unshift($jcd_categories, "" );

            //Access the WordPress Pages via an Array
            $jcd_pages = array();
            $jcd_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
            foreach ($jcd_pages_obj as $jcd_page) {
                    $jcd_pages[$jcd_page->ID] = $jcd_page->post_title;
            }
            // $jcd_pages_tmp = array_unshift($jcd_pages, "Select a page:" );

            //More Options
            $other_entries = array( "Select a number:","4","8","12" );
            $number_entries = array( "Select a number:","1","2","3","4","5","6","7","8","9" );


            // THIS IS THE DIFFERENT FIELDS
            $options = array();


            /**
             * General Settings Section
             */
            // $options[] = array(
            //  "name" => "General Settings",
            //  "type" => "heading",
            //  "icon" => "general",
            // );

            // $options[] = array(
            //  "name" => "Logo",
            //  "desc" => "Upload or enter URL for Logo",
            //  "id" => $shortname."_logo",
            //  "std" => get_template_directory_uri() . '/images/logo.svg',
            //  "type" => "upload"
            // );

            // $options[] = array(
            //  "name" => "Copyrights",
            //  "desc" => "Copyrights Text.",
            //  "id" => $shortname."_copyrights_text",
            //  "std" => "",
            //  "type" => "textarea"
            // );

            // $options[] = array(
            //  "name" => "Tracking Code",
            //  "desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
            //  "id" => $shortname."_google_analytics",
            //  "std" => "",
            //  "type" => "textarea"
            // );

            /**
             * Social Settings
             */
            // $options[] = array(
            //  "name" => "Social Links",
            //  "type" => "heading",
            //  "icon" => "connect"
            // );

            // $options[] = array(
            //  "name" => "Facebook URL",
            //  "desc" => "Enter your Facebook URL.",
            //  "id" => $shortname."_social_facebook",
            //  "std" => "",
            //  "type" => "text"
            // );

            // $options[] = array(
            //  "name" => "Twitter URL",
            //  "desc" => "Enter your Twitter URL.",
            //  "id" => $shortname."_social_twitter",
            //  "std" => "",
            //  "type" => "text"
            // );

            // $options[] = array(
            //  "name" => "Instagram URL",
            //  "desc" => "Enter your Instagram URL.",
            //  "id" => $shortname."_social_instagram",
            //  "std" => "",
            //  "type" => "text"
            // );

            /*$options[] = array(
                "name" => "Google+ URL",
                "desc" => "Enter your Google+ URL.",
                "id" => $shortname."_social_google-plus",
                "std" => "",
                "type" => "text"
            );*/

            /*$options[] = array(
                "name" => "LinkedIn URL",
                "desc" => "Enter your LinkedIn URL.",
                "id" => $shortname."_social_linkedin",
                "std" => "",
                "type" => "text"
            );*/

            /**
             * Dynamic Images Settings
             */
            $options[] = array(
                "name" => "Dynamic Images",
                "type" => "heading",
                "icon" => "image"
            );

            $options[] = array(
                "name" => 'Dynamic Image Resizing',
                "desc" => "",
                "id" => $shortname."_wpthumb_notice",
                "std" => 'Method of dynamically resizing the thumbnails in the theme, <strong>WP Post Thumbnail</strong>.',
                "type" => "info"
            );

            $options[] = array(
                "name" => "WP Post Thumbnail",
                "desc" => "Use WordPress post thumbnail to assign a post thumbnail. Will enable the <strong>Featured Image panel</strong> in your post sidebar where you can assign a post thumbnail.",
                "id" => $shortname."_post_image_support",
                "std" => "true",
                "class" => "collapsed",
                "type" => "checkbox"
            );

            $options[] = array(
                "name" => "WP Post Thumbnail - Dynamic Image Resizing",
                "desc" => "The post thumbnail will be dynamically resized using native WP resize functionality. <em>(Requires PHP 5.2+)</em>",
                "id" => $shortname."_pis_resize",
                "std" => "true",
                "class" => "hidden",
                "type" => "checkbox"
            );

            $options[] = array(
                "name" => "WP Post Thumbnail - Hard Crop",
                "desc" => "The post thumbnail will be cropped to match the target aspect ratio (only used if 'Dynamic Image Resizing' is enabled).",
                "id" => $shortname."_pis_hard_crop",
                "std" => "true",
                "class" => "hidden last",
                "type" => "checkbox"
            );

            $options[] = array(
                "name" => "Automatic Image Thumbnail",
                "desc" => "If no thumbnail is specifified then the first uploaded image in the post is used.",
                "id" => $shortname."_auto_img",
                "std" => "true",
                "type" => "checkbox"
            );

            /*$options[] = array(
                "name" => "Default Image",
                "desc" => "Choose your default image, if the post has no image.",
                "id" => $shortname."_default_image",
                "type" => 'upload',
                "std" => get_template_directory_uri().'/images/thumb-large.jpg'
            );*/

            $options[] = array(
                "name" => "Add thumbnail to RSS feed",
                "desc" => "Add the the image uploaded via your Custom Settings panel to your RSS feed",
                "id" => $shortname."_rss_thumb",
                "std" => "false",
                "type" => "checkbox"
            );



            // Add extra options through function
            if ( function_exists( "jcd_options_add") )
                $options = jcd_options_add($options);

            if ( get_option( 'jcd_template') != $options) update_option( 'jcd_template',$options);
            if ( get_option( 'jcd_themename') != $themename) update_option( 'jcd_themename',$themename);
            if ( get_option( 'jcd_shortname') != $shortname) update_option( 'jcd_shortname',$shortname);


            // Metabox Options
            // Start name with underscore to hide custom key from the user
            $jcd_metaboxes = array();

            global $post;

            // Add extra metaboxes through function
            if ( function_exists( "jcd_metaboxes_add") )
                $jcd_metaboxes = jcd_metaboxes_add($jcd_metaboxes);

            if ( get_option( 'jcd_custom_template' ) != $jcd_metaboxes)
                update_option( 'jcd_custom_template', $jcd_metaboxes );

    } // endof jcd_options
}


// Add options to admin_head
add_action( 'admin_head', 'jcd_options' );


/* ===================================================================
  CUSTOM FIELDS
=================================================================== */
