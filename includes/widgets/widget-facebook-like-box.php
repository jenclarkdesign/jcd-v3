<?php
/**
 * JCD Widget Class
 */
class JCD_Widget extends WP_Widget {
	function __construct( $base_id, $widget_title, $widget_options ) {
		parent::__construct( $base_id, $widget_title, $widget_options );
	}

	/**
	 * Save Widget
	 */
	function update( $new_instance, $old_instance ) {
		return $new_instance;
	}

	/**
	 * Enforce Defaults
	 */
	function jcd_enforce_defaults( $instance ) {
		$defaults = $this->jcd_get_settings();
		$instance = wp_parse_args( $instance, $defaults );

		return $instance;
	}

	/**
	 * Provides an array of the settings with the setting name as the
	 * key and the default value as the value
	 * This cannot be called get_settings() or it will override
	 * WP_Widget::get_settings()
	 */
	function jcd_get_settings() {
		// Set the default to a blank string
		$settings = array_fill_keys( $this->settings, '' );
		// Now set the more specific defaults
		return $settings;
	}
}


/**
 * Facebook Like Box
 */
class JCD_Facebook_Like_Box extends JCD_Widget {
	var $settings = array( 'title', 'page_url' );

	function __construct() {
		$widget_ops = array(
			'classname' => 'widget-facebook-like',
			'description' => 'Show Facebok Like Box',
		);
		parent::__construct( 'jcd_widget_facebook_like', __('JCD - Facebook Like Box', 'jcd'), $widget_ops );

		add_action( 'wp_footer', array( $this, 'load_facebook_sdk' ) );
	}

	/**
	 * Load Facebook SDK
	 */
	function load_facebook_sdk() {
		if ( is_active_widget( false, false, $this->id_base, true ) ) : ?>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.4";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
		<?php endif;
	}

	/**
	 * Render Widget
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		echo $before_widget;

		if ( $title ) {
			echo $before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title;
		}
		?>

		<div class="fb-page" data-href="<?php echo $page_url; ?>" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
			<div class="fb-xfbml-parse-ignore">
				<blockquote cite="<?php echo $page_url; ?>" class="text-center">
					<a href="<?php echo $page_url; ?>" class="button button-primary button-bold">Our Facebook Page</a>
				</blockquote>
			</div>
		</div>

		<?php
		echo $after_widget;
	}


	/**
	 * Render Form
	 */
	function form( $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('page_url'); ?>"><?php _e('Facebook Page URL:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('page_url'); ?>"  value="<?php echo esc_attr( $page_url ); ?>" class="widefat" id="<?php echo $this->get_field_id('page_url'); ?>" />
		</p>
		<?php
	}
}

register_widget( 'JCD_Facebook_Like_Box' );
