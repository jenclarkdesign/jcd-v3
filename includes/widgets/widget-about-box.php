<?php
/**
 * About Box Widget
 */
class JCD_About_Box extends JCD_Widget {
	var $settings = array( 'photo', 'description', 'readmore_link' );

	function __construct() {
		$widget_ops = array(
			'classname' => 'widget-about-box',
			'description' => 'Show About Box',
		);
		parent::__construct( 'jcd_widget_about_box', __('JCD - About Box', 'jcd'), $widget_ops );
	}

	/**
	 * Render Widget
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		echo $before_widget; ?>

		<?php if( $photo ) : ?>
			<div class="about-photo">
				<img src="<?php echo $photo; ?>">
			</div>
		<?php endif; ?>

		<?php if( $description ) : ?>
			<div class="entry-content"><?php echo apply_filters( 'the_content', $instance['description'] ); ?></div>
		<?php endif; ?>

		<?php if( $readmore_link ) : ?>
			<a class="readmore-link" href="<?php echo $readmore_link; ?>">Learn more</a>
		<?php endif; ?>

		<?php echo $after_widget;
	}

	/**
	 * Render Form
	 */
	function form( $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('photo'); ?>"><?php _e('Photo URL:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('photo'); ?>"  value="<?php echo esc_attr( $photo ); ?>" class="widefat" id="<?php echo $this->get_field_id('photo'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description Text:','jcd'); ?></label>
			<textarea name="<?php echo $this->get_field_name('description'); ?>" class="widefat" id="<?php echo $this->get_field_id('description'); ?>"><?php echo esc_attr( $description ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('readmore_link'); ?>"><?php _e('Readmore URL:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('readmore_link'); ?>"  value="<?php echo esc_attr( $readmore_link ); ?>" class="widefat" id="<?php echo $this->get_field_id('readmore_link'); ?>" />
		</p>
		<?php
	}

	function jcd_get_settings() {
		// Set the default to a blank string
		$settings = array_fill_keys( $this->settings, '' );
		// Now set the more specific defaults
		$settings['photo'] = get_template_directory_uri() . '/images/about-photo.jpg';
		return $settings;
	}
}

register_widget( 'JCD_About_Box' );
