<?php
/**
 * Donation Widget
 */
class JCD_Donation_Widget extends WP_Widget {
	var $settings = array( 'title', 'donation_url' );

	function __construct() {
		$widget_ops = array(
			'classname' => 'widget-donation',
			'description' => 'Make a donation to the Peter MacCallum Cancer Foundation Widget',
		);
		parent::__construct( false, __('JCD - Donation', 'jcd'), $widget_ops );
	}


	/**
	 * Render Widget
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );
		extract( $instance, EXTR_SKIP );

		echo $before_widget;

		if ( $title ) {
			echo $before_title . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $after_title;
		}

		echo '<p class="text-center"><img src="'. get_template_directory_uri() . '/images/logo-peter-mac.png' .'"></p>';
		echo '<p class="text-center">Make a donation to the Peter MacCallum Cancer Foundation</p>';

		if( $donation_url ) {
			echo '<div class="text-center"><a href="'. $donation_url .'" class="button button-primary button-bold">Donate</a></div>';
		}

		echo $after_widget;
	}


	/**
	 * Save Widget
	 */
	function update( $new_instance, $old_instance ) {
		return $new_instance;
	}


	/**
	 * Render Form
	 */
	function form( $instance ) {
		$instance = $this->jcd_enforce_defaults( $instance );
		extract( $instance, EXTR_SKIP ); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title (optional):','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo esc_attr( $title ); ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('donation_url'); ?>"><?php _e('Donation URL:','jcd'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('donation_url'); ?>"  value="<?php echo esc_attr( $donation_url ); ?>" class="widefat" id="<?php echo $this->get_field_id('donation_url'); ?>" />
		</p>
		<?php
	}

	/**
	* Enforce Defaults
	*/
	function jcd_enforce_defaults( $instance ) {
		$defaults = $this->jcd_get_settings();
		$instance = wp_parse_args( $instance, $defaults );

		return $instance;
	}


	/**
	* Provides an array of the settings with the setting name as the
	* key and the default value as the value
	* This cannot be called get_settings() or it will override
	* WP_Widget::get_settings()
	*/
	function jcd_get_settings() {
		// Set the default to a blank string
		$settings = array_fill_keys( $this->settings, '' );
		// Now set the more specific defaults
		return $settings;
	}
}

register_widget( 'JCD_Donation_Widget' );
