<?php
/**
 * Register Sidebar
 */
function jcd_widgets_init() {

	register_sidebar( array(
		'name'          => 'Widgets',
		'id'            => 'main-widget',
		'description'   => "Main Widget",
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget__title">',
		'after_title'   => '</h3>'
	) );

	/*register_sidebar( array(
		'name' => 'Footer Widgets',
		'id' => 'footer-widget',
		'description' => "Widget on footer section",
		'before_widget' => '<aside id="%1$s" class="widget column col6 %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>'
	) );*/

}
add_action( 'widgets_init', 'jcd_widgets_init' );
