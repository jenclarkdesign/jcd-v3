<?php
/**
 * WP Instagram main class
 */
class WP_Instagram {

  /**
   * Constructor
   */
  function __construct() {
    $this->define_constant();
    $this->include_files();

    // Register cron job
    /*if( !wp_next_scheduled( 'wp_instagram_sync' ) ) {
      wp_schedule_event( current_time( 'timestamp', wp_timezone_override_offset() ), 'hourly', 'wp_instagram_sync' );
    }*/
  }


  /**
   * Define constant
   */
  function define_constant() {
    if( !defined( 'WP_INSTAGRAM_DIR' ) )
      define( 'WP_INSTAGRAM_DIR', get_template_directory() . '/includes/wp-instagram/');
  }

  /**
   * Include required files
   */
  function include_files() {
    require_once( WP_INSTAGRAM_DIR . 'classes/class-instagram-api.php' );
    // require_once( WP_INSTAGRAM_DIR . 'classes/class-post-type.php' );
    require_once( WP_INSTAGRAM_DIR . 'classes/class-import.php' );
    require_once( WP_INSTAGRAM_DIR . 'classes/class-admin.php' );
    // require_once( WP_INSTAGRAM_DIR . 'classes/class-sync.php' );
  }

}

new WP_Instagram;
