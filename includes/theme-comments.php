<?php
/*-----------------------------------------------------------------------------------*/
/* JCD - List Comment */
/*-----------------------------------------------------------------------------------*/
function jcd_list_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	$GLOBALS['comment_depth'] = $depth;
?>

	<li <?php comment_class(); ?>>
		<div id="comment-<?php comment_ID(); ?>" class="comment-body">

			<?php if( '' == $comment->comment_type ) : ?>
				<div class="comment-avatar">
					<?php
						$avatar_email = get_comment_author_email();
						$avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( $avatar_email, 80 ) );
						echo $avatar;
					?>
				</div>
			<?php endif; ?>

			<div class="comment-content">
				<?php if ( '0' == $comment->comment_approved ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'jcd' ); ?></em>
				<?php endif; ?>

				<div class="comment-author"><?php echo get_comment_author_link(); ?></div>

				<div class="comment-text"><?php comment_text(); ?></div>

				<div class="comment-meta">
					<?php printf( __( '%1$s at %2$s', 'jcd' ), get_comment_date(), get_comment_date('h:s a') ); ?>

					&nbsp;&bull;&nbsp;

					<?php comment_reply_link( array_merge( $args, array(
						'reply_text' => __( 'Reply', 'jcd' ),
						'depth' => $depth,
						'max_depth' => $args['max_depth']
					) ) ); ?>
				</div>
			</div>

		</div>

<?php }


// Produces an avatar image with the hCard-compliant photo class
function commenter_link() {
 $commenter = get_comment_author_link();
 if ( ereg( '<a[^>]* class=[^>]+>', $commenter ) ) {
	$commenter = ereg_replace( '(<a[^>]* class=[\'"]?)', '\\1url ' , $commenter );
 } else {
	$commenter = ereg_replace( '(<a )/', '\\1class="url "' , $commenter );
 }
 $avatar_email = get_comment_author_email();
 $avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( $avatar_email, 45 ) );
 echo $avatar;
} // end commenter_link

// Custom callback to list pings
function custom_pings($comment, $args, $depth) {
			 $GLOBALS['comment'] = $comment;
				?>
			<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
			 <div class="comment-author"><?php printf(__('By %1$s on %2$s at %3$s', 'jcd'),
				 get_comment_author_link(),
				 get_comment_date(),
				 get_comment_time() );
				 edit_comment_link(__('Edit', 'jcd'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
		<?php if ('0' == $comment->comment_approved) _e('\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n', 'jcd'); ?>
						<div class="comment-content">
			 <?php comment_text(); ?>
	 </div>
<?php } // end custom_pings
?>
