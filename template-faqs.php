<?php
/**
 * Template Name: FAQS
 */

get_header( );

get_template_part( 'partials/component/page', 'heading' ); 
    
if ( have_posts()) : while (have_posts() ) : the_post(); ?>
    <div class="main-content-section wrapper block-section">
        <div class="grid">
            <div class="grid__item large--ten-twelfths push--large--one-twelfth">
                <div class="entry-content">
                    <?php the_content(); ?>
                </div>
                    
                <?php get_template_part( 'partials/sections/faqs-section' ); ?>		
            </div>
        </div>
    </div>
<?php endwhile; endif;

get_footer();