<?php get_header(); ?>

<?php get_template_part( 'partials/component/page-heading' ); ?>

<div class="main-content-section">
    <div class="wrapper">
        <div class="grid">
            <div class="grid__item large--eight-twelfths">
                <?php if ( have_posts() ) : ?>
                    <div class="blog-list grid grid-uniform js-infinite-scroll">
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php 
                            global $loop;
                            $loop['columns'] = 2;
                            get_template_part( 'partials/content/blog-item' );
                            ?>
                        <?php endwhile; ?>
                    </div>

                    <div class="js-infinite-scroll-pagination clearfix">
                        <span class="prev float-left"><?php previous_posts_link(); ?></span>
                        <span class="js-infinite-scroll-pagination__next next float-right"><?php next_posts_link(); ?></span>
                    </div>

                    <div class="text-align-center">
                        <button class="button button--primary button--outline font-weight-bold button--width-wide button--wide js-infinite-scroll-button">
                            <?php _e('Load More', 'jcd'); ?>
                        </button>
                    </div>
                <?php endif; ?>
            </div>

            <div class="grid__item large--four-twelfths">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>
<!-- /.main-content-section -->

<?php get_footer(); ?>