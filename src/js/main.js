import "./modules/general";
import "./modules/mobileMenu";
import "./modules/horizontalScrollMenu";
import "./modules/infiniteScroll";
import "./modules/projectGallery";
import "./modules/stickyMenu";
import "./modules/homeParallax";
// import "./modules/projectFilters";
// import "./modules/maps";