import $ from "jquery";
import { $body, $window } from "../global";

/**
 * Make inline list responsive by append "More" dropdown at the end of the list
 * when its width exceed the maximum width
 */
class ResponsiveList {
	constructor($menu) {
		this.el = {
			'$menu': $menu
		};
		this.navWidth = 0;
		this.moreWidth = 0;
		this.availableSpace = 0;
		this.isMenuActive = false;

		this.calcWidth = this.calcWidth.bind(this);
		this.toggleMenu = this.toggleMenu.bind(this);
		this.hideMenu = this.hideMenu.bind(this);
	}

	setupElements() {
		this.el.$menuWrapper = this.el.$menu.parent();
		this.el.$moreMenu = this.el.$menu.find('.more');
		this.el.$moreSubmenu = this.el.$moreMenu.find('ul');
	}

	setupVars() {
		this.moreWidth = this.el.$moreMenu.outerWidth(true);
		this.availableSpace = this.el.$menuWrapper.width() - this.moreWidth;
		this.navWidth = this.el.$menu.outerWidth(true);
	}

	calcWidth() {
		this.setupVars();

		if(this.navWidth >= this.availableSpace) {
			const $lastItem = this.el.$menu.children('li:not(.more)').last();
			$lastItem.attr('data-width', $lastItem.outerWidth(true));
			$lastItem.prependTo( this.el.$moreSubmenu );
			this.calcWidth();
		} else {
			const $firstMoreElement = this.el.$moreSubmenu.find('li').first();
			if(this.navWidth + $firstMoreElement.data('width') < this.availableSpace) {
				$firstMoreElement.insertBefore(this.el.$moreMenu);
			}
		}

		if(this.el.$moreMenu.find('li').length > 0) {
			this.el.$moreMenu.css('display', 'inline-block');
		} else {
			this.el.$moreMenu.css('display', 'none');
		}
	}

	eventBinding() {
		this.el.$moreMenu.children('a').on( 'click', this.toggleMenu );
		$window.on('resize load', this.calcWidth);
		$body.on('click', this.hideMenu);
	}

	hideMenu() {
		this.isMenuActive = true;
		this.toggleMenu();
	}

	toggleMenu() {
		if(this.isMenuActive) {
			this.el.$moreMenu.removeClass('active');
		} else {
			this.el.$moreMenu.addClass('active');
		}

		this.isMenuActive = !this.isMenuActive;
		return false;
	}

	init() {
		this.setupElements();
		this.calcWidth();
		this.eventBinding();
	}
}

const $menu = $('.category-list-block > ul');
if ( $menu.length ) {
	const List = new ResponsiveList($menu);
	List.init();
}
