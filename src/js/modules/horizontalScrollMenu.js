import $ from 'jquery';
import requestAnimationFrame from "requestAnimationFrame";

/**
 * Horizontal Scroll Menu
 * https://benfrain.com/a-horizontal-scrolling-navigation-pattern-for-touch-and-mouse-with-moving-current-indicator/
 */
class HorizontalScrollMenu {
  constructor(opts) {
    this.container = opts.container;
    this.content = opts.content;
    this.lastScrollPosition = 0;
    this.isTicking = false;
    this.isScrolling = false;
    this.scrollDistance = 150;
    this.scrollDirection = '';

    this.$leftButton = $(this.container).siblings(".js-horz-left-button");
    this.$rightButton = $(this.container).siblings(".js-horz-right-button");

    if (this.container && this.content) {
      this.setContainerAttr(0);
      this.container.addEventListener('scroll', this.menuScroll);
      this.content.addEventListener('transitionend', this.onScrollEnd, false);
      this.$leftButton.on('click', this.scrollLeft);
      this.$rightButton.on('click', this.scrollRight);
    }
  }

  menuScroll = () => {
    this.lastScrollPosition = window.scrollY;
    if (!this.isTicking) {
      requestAnimationFrame(() => {
        this.setContainerAttr(this.lastScrollPosition);
        this.isTicking = false;
      });
    }
    this.isTicking = true;
  };

  scrollLeft = () => {
    if (this.isScrolling === true) {
      return;
    }

    if (this.scrollDirection === 'left' || this.scrollDirection === 'both') {
      // Find how far this panel has been scrolled
      let availableScrollLeft = this.container.scrollLeft;
      // If the space available is less than two lots of our desired distance, just move the whole amount
      // otherwise, move by the amount in the settings
      if (availableScrollLeft < this.scrollDistance * 2) {
        this.content.style.transform = `translateX(${availableScrollLeft}px)`;
      } else {
        this.content.style.transform = `translateX(${this.scrollDistance}px)`;
      }

      // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
      this.content.classList.remove('no-transition');
      this.scrollDirection = 'left';
      this.isScrolling = true;
    }

    this.scrollDirection = this.determineOverflow(this.content, this.container);
    this.container.setAttribute('data-overflowing', this.scrollDirection);
  };

  scrollRight = () => {
    if (this.isScrolling === true) {
      return;
    }

    if (this.scrollDirection === 'right' || this.scrollDirection === 'both') {
      // Get the right edge of the container and content
      let navbarRightEdge = this.content.getBoundingClientRect().right;
      let navbarScrollerRightEdge = this.container.getBoundingClientRect().right;
      // Now we know how much space we have available to scroll
      let availableScrollRight = Math.floor(navbarRightEdge - navbarScrollerRightEdge);
      // If the space available is less than two lots of our desired distance, just move the whole amount
      // otherwise, move by the amount in the settings
      if (availableScrollRight < this.scrollDistance * 2) {
        this.content.style.transform = `translateX(-${availableScrollRight}px)`;
      } else {
        this.content.style.transform = `translateX(-${this.scrollDistance}px)`;
      }

      // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
      this.content.classList.remove('no-transition');
      this.scrollDirection = 'right';
      this.isScrolling = true;
    }

    
    this.scrollDirection = this.determineOverflow(this.content, this.container);
    this.container.setAttribute('data-overflowing', this.scrollDirection);
  };

  onScrollEnd = () => {
    // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
    let styleOfTransform = window.getComputedStyle(this.content, null);
    let tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
    // If there is no transition we want to default to 0 and not null
    let amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
    this.content.style.transform = "none";
    this.content.classList.add("no-transition");
    // Now lets set the scroll position
    if (this.scrollDirection === "left") {
      this.container.scrollLeft = this.container.scrollLeft - amount;
    } else {
      this.container.scrollLeft = this.container.scrollLeft + amount;
    }
    this.isScrolling = false;
  };

  setContainerAttr(lastScrollPosition) {
    this.scrollDirection = this.determineOverflow(this.content, this.container);
    this.container.setAttribute('data-overflowing', this.scrollDirection);
  }

  determineOverflow(content, container) {
    let containerMetrics = container.getBoundingClientRect();
    let containerMetricsRight = Math.floor(containerMetrics.right);
    let containerMetricsLeft = Math.floor(containerMetrics.left);
    let contentMetrics = content.getBoundingClientRect();
    let contentMetricsRight = Math.floor(contentMetrics.right);
    let contentMetricsLeft = Math.floor(contentMetrics.left);
    if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
      return "both";
    } else if (contentMetricsLeft < containerMetricsLeft) {
      return "left";
    } else if (contentMetricsRight > containerMetricsRight) {
      return "right";
    } else {
      return "none";
    }
  }
}

if ( $('.js-horz-container').length ) {
  $('.js-horz-container').each((index, item) => {
    new HorizontalScrollMenu({
      container: $(item)[0],
      content: $(item).find('.js-horz-content')[0]
    });
  });
}