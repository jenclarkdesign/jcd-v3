import $ from 'jquery';
import InfiniteScroll from 'infinite-scroll';

if ($('.js-infinite-scroll').length) {
  const $next = $('.js-infinite-scroll-pagination__next a');

  // Hide the load more if reached the end of the page
  if (!$next.length) {
    $('.js-infinite-scroll-button').hide();
  }

  if ($next.length) {
    const infScroll = new InfiniteScroll('.js-infinite-scroll', {
      path: '.js-infinite-scroll-pagination__next a',
      append: '.blog-item',
      checkLastPage: true,
      hideNav: '.js-infinite-scroll-pagination',
      button: '.js-infinite-scroll-button',
      scrollThreshold: false,
      debug: false,
    });
  }
}
