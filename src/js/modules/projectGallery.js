import $ from "jquery";
import PhotoSwipe from 'photoswipe';
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default';

class PortfolioImage {
  constructor() {
    this.el = {};
    this.setupElements();
    this.eventBinding();
  }

  setupElements() {
    this.el.$images = $('.project-images');
    this.el.$photoswipeEl = $('.pswp');
  }

  eventBinding() {
    if ( this.el.$images.length > 0 ) {
      this.el.$images.on( 'click', 'a', (e) => this.onThumbClick(e) );
    }
  }

  onThumbClick(e) {
    e.preventDefault();

    const $link = $(e.currentTarget),
        index = $link.data('index'),
        $gallery = $link.closest('.project-images');

    this.openPhotoSwipe( index, $gallery );
  }

  openPhotoSwipe( index, $gallery ) {
    const items = this.parseThumbnaileElements( $gallery ),
        options = {
          galleryUID: $gallery.data('pswp-uid'),
          index,
          getThumbBoundsFn: (index) => {
            const $thumb = items[index].el.find('img'),
                offset = $thumb.offset();

            return {
              x: offset.left,
              y: offset.top,
              w: $thumb.width()
            };
          }
        },
        pswpElement = this.el.$photoswipeEl[0],
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options ) ;

    gallery.init();
  }

  parseThumbnaileElements( $gallery ) {
    const items = [];

    $gallery.find('.project-images__item').each((index, item) => {
      const $el = $(item);
      const data = {
        src: $el.attr('href'),
        w: parseInt( $el.data('width'), 10 ),
        h: parseInt( $el.data('height'), 10 ),
        el: $el,
        title: $el.find('img').attr('alt')
      };

      items.push( data );
    });

    return items;
  }
}

new PortfolioImage();