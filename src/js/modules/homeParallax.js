import * as basicScroll from '../libs/basicscroll.min';
import ScrollReveal from 'scrollreveal';

/**
 * Hero Parallax
 */
document.querySelectorAll('.js-hero-parallax').forEach((elem, i) => {
  const modifier = elem.getAttribute('data-modifier');
  const direction = elem.getAttribute('data-direction');
  const dir = direction === 'up' ? '-' : '';

  basicScroll.create({
    elem: document.getElementById('hero-section'),
    from: 0,
    to: "bottom-top",
    direct: elem,
    props: {
      "--ty": {
        from: "0",
        to: `${dir}${ 10 * modifier }px`
      }
    }
  }).start();
});


/**
 * Image List Parallax
 */
document.querySelectorAll('.link-list-item').forEach((item) => {  
  item.querySelectorAll('.js-service-parallax').forEach((elem) => {
    const modifier = elem.getAttribute("data-modifier");
    const timing = elem.getAttribute('data-timing');
    const direction = elem.getAttribute('data-direction');
    const dir = direction === 'up' ? '-' : '';

    basicScroll.create({
      elem: item,
      from: 'top-middle',
      to: 'bottom-top',
      direct: elem,
      props: {
        "--ty": {
          from: "0",
          to: `${dir}${ 10 * modifier }px`,
          // timing
        }
      }
    }).start();
  });
});


/**
 * Project Scroll Reveal
 */
const sr = ScrollReveal();

sr.reveal(".js-box-item", {
  origin: "bottom",
  distance: "30px",
  rotate: { x: 0, y: 0, z: 0 },
  scale: 1,
  delay: 50
});