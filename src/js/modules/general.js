import $ from "jquery";
import debounce from "lodash.debounce";
import fitvids from "fitvids";
// import requestAnimationFrame from "requestAnimationFrame";
import "jquery-match-height";
import "slick-carousel";
import "velocity-animate";

import { $body, $window } from "../global";
import { hasAdminBar } from "../services/helper";

/**
 * If browser doesn't support svg as img tag, replace with PNG file
 */
if (!Modernizr.svgasimg) {
  var $logo = $(".logo__img[src$='.svg']");
  $logo.attr("src", $logo.attr('src').replace('.svg', '.png'));
}


/**
 * Wrap Select dropdown with a Div
 */
$('.form-row select').wrap('<div class="select-field" />');

// Support for gravity form
$(document).on('gform_post_render', () => {
  $('.gform_body select').wrap('<div class="select-field" />');
});

/**
 * Match Height
 */
// $('.project-categories-item__description').matchHeight();
// $('.about-section__item').matchHeight();


/**
 * Responsive Video
 */
fitvids('.entry-content');


/**
 * Set ATF Section height
 */
const $headerSection = $('.topbar-section');
const $atfSection = $('.hero-section__image');
function setATFHeight() {
  $atfSection.removeAttr('style');

  // if ( $window.width() >= 1024 ) {
    const windowHeight = $window.height();
    const headerHeight = $headerSection.outerHeight(true);
    const adminbarHeight = hasAdminBar() ? 32 : 0;
    let heroHeight = windowHeight - adminbarHeight - headerHeight;
  
    $atfSection.height(heroHeight);
  // }
}

if ($atfSection.length > 0) {
  setATFHeight();
  $window.on('resize', debounce(setATFHeight, 200));
}


/**
 * Accordion Section
 */
$('.accordion-item').addClass('closed');
$(".accordion-item__button, .accordion-item__title").on("click", e => {
  e.preventDefault();

  const $trigger = $(e.currentTarget);
  const $item = $trigger.closest('.accordion-item');
  const slideDirection = $item.hasClass('closed') ? 'slideDown' : 'slideUp';

  $item.find('.accordion-item__description').velocity(slideDirection);
  $item.toggleClass("closed");
});


/**
 * Minimal Slider
 */
$('.js-minimal-slider').each((index, item) => {
  $(item).slick({
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    dots: true,
    appendDots: $(item).siblings(".minimal-slider__nav")
  });
});