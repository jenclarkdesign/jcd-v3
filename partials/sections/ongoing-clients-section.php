<?php
$title = get_field('ongoing_clients_title');
$clients = get_field('ongoing_client_list');

if ($clients) : ?>
    <div class="block-section ongoing-client-section">
        <div class="wrapper">
            <?php if ($title) : ?>
                <h3 class="block-section__title text-align-center membership-section__title "><?php echo $title; ?></h3>
            <?php endif; ?>
    
            <div class="ongoing-client-list grid">
                <?php foreach ($clients as $client) : ?>
                    <?php if ($client['url']) : ?>
                        <a href="<?php echo esc_url($client['url']); ?>" class="ongoing-client-list-item grid__item large--three-twelfths medium--three-twelfths" target="_blank" rel="nofollow">
                    <?php else : ?>
                        <div class="ongoing-client-list-item grid__item large--three-twelfths medium--three-twelfths">
                    <?php endif; ?>
                        <?php echo wp_get_attachment_image($client['image']['id'], 'about-thumb'); ?>
                    <?php if ($client['url']) : ?>
                        </a>
                    <?php else : ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- /.block-section -->
<?php endif; ?>