<?php
$instagram_title = get_field( 'instagram_title' );
$instagram_token = get_option('wp_instagram_access_token');
$instagram_count = ( get_field('instagram_posts_per_page') ) ? get_field('instagram_posts_per_page') : 11 ;

if( $instagram_token ) :
    $instagram_api = new WP_Instagram_API( $instagram_token );
    $user_media = $instagram_api->user_media(array(
        'user_id' => 'self',
        'count'   => $instagram_count,
    ));

    $user_url = 'http://instagram.com/';
    $user = $instagram_api->get( "{$instagram_api->endpoint}users/self?access_token={$instagram_api->access_token}" );
    if( $user && $user->meta->code == 200 ) {
        $user_url .= $user->data->username;
    }

    if( $user_media && $user_media->meta->code == 200 ) : ?>

        <div class="instagram-section block-section">
            <div class="wrapper">
                <h3 class="block-section__title instagram-section__title text-align-center"><?php echo $instagram_title; ?></h3>
                
                <div class="grid grid-uniform instagram-item-list">
                    <?php foreach ($user_media->data as $data) : ?>
                        <div class="js-box-item grid__item instagram-item one-half block-square large--two-twelfths medium--four-twelfths">
                            <div class="block-square__inner">
                                <a target="_blank" href="<?php echo esc_attr($data->link); ?>" class="block-square__center">
                                    <img src="<?php echo $data->images->low_resolution->url; ?>" alt="<?php echo $data->caption->text; ?>">
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <div class="js-box-item grid__item instagram-item one-half block-square large--two-twelfths medium--four-twelfths more-link">
                        <div class="block-square__inner">
                            <a target="_blank" href="<?php echo $user_url; ?>" class="block-square__center">
                                <?php _e('Follow me on Instagram', 'jcd'); ?> &rarr;
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        

    <?php endif;
endif;