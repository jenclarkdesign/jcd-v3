<?php
$title          = get_field( 'projects_title' ); 
$posts_per_page = ( get_field( 'projects_posts_per_page' ) ) ? get_field( 'projects_posts_per_page') : 12;
$button_text    = get_field( 'projects_button_text' ); 

$args = array(
    'post_type'      => 'projects',
    'post_status'    => array( 'publish' ),
    'posts_per_page' => $posts_per_page,
);
$projects = new WP_Query( $args );

if ( $projects->have_posts() ) : ?>
    <div class="project-section block-section">
        <div class="wrapper">
            <?php if ($title) : ?>
                <h3 class="text-center block-section__title project-section__title"><?php echo $title; ?></h3>
            <?php endif; ?>

            <div class="grid portfolio-list">
                <?php while ( $projects->have_posts() ) : $projects->the_post(); ?>
                    <?php get_template_part('partials/content/portfolio-item'); ?>
                <?php endwhile; ?>
            </div>

            <div class="text-center">
                <a class="button button--primary button--outline font-weight-bold project-section__button" href="<?php echo get_post_type_archive_link( 'projects' ); ?>">
                    <?php echo $button_text; ?>
                </a>
            </div>
        </div>
    </div>
<?php wp_reset_postdata(); endif; ?>
