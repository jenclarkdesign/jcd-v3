<?php if( have_rows( 'about_content' ) ) : ?>
    <div class="about-content clearfix">
        <?php while( have_rows( 'about_content' ) ) {
            the_row();

            switch (get_row_layout()) {
                case 'image_left_section':
                    get_template_part('partials/sections/image-left-section');
                break;

                case 'image_right_section':
                    get_template_part('partials/sections/image-right-section');
                break;
            }
        }
        ?>
    </div>
<?php endif; ?>