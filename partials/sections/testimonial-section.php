<?php 
$testimonials_title = get_field( 'testimonials_title', get_option( 'page_on_front') );
$testimonials_page  = get_field( 'testimonials_page', 'option');
$testimonials       = get_field( 'clients', $testimonials_page );

if ( !$testimonials_title ) {
    $testimonials_title = __('Testimonials', 'jcd');
}

if ( $testimonials ) : ?>
    <div class="testimonial-section block-section">
        <div class="wrapper">
            <h3 class="testimonial-section__title block-section__title"><?php echo $testimonials_title; ?></h3>
            
            <div class="grid">
                <div class="grid__item large--eight-twelfths medium--eight-twelfths">
                    <div class="minimal-slider-wrapper testimonial-list-wrapper">
                        <div class="minimal-slider testimonial-list js-minimal-slider">
                            <?php foreach( $testimonials as $testimony ) : ?>
                                <div class="minimal-slider__item testimonial-item">
                                    <?php if ( $testimony['testimonial'] ) : ?>
                                        <div class="testimonial-item__text"><?php echo $testimony['testimonial']; ?></div>
                                    <?php endif; ?>

                                    <div class="testimonial-item__meta">
                                        <?php if ($testimony['image']) : ?>
                                            <div class="testimonial-item__image">
                                                <?php echo wp_get_attachment_image( $testimony['image']['id'], 'thumbnails' ); ?>
                                            </div>
                                        <?php endif; ?>

                                        <div class="testimonial-item__meta-data">
                                            <?php if ( $testimony['name'] ) : ?>
                                                <h4 class="testimonial-item__name"><?php echo $testimony['name']; ?></h4>
                                            <?php endif; ?>

                                            <?php if ( $testimony['company'] ) : ?>
                                                <div class="testimonial-item__company"><?php echo $testimony['company']; ?></div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <nav class="minimal-slider__nav"></nav>
                    </div>
                </div>

                <div class="grid__item large--four-twelfths push--large--one-twelfth medium--four-twelfths testimonial-rating">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/google.svg" alt="Google" />
                    <h4 class="testimonial-rating__title">Review Summary</h4>
                    <div class="testimonial-rating__rate">4.5</div>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/star.svg" alt="4.5 stars" />
                </div>
            </div>
        </div>
    </div>
    <!-- /.testimonial-section block-section -->
<?php endif; ?>
