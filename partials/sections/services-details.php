<?php if( $services = get_field( 'design-service' ) ): ?>
    <div class="services-list">
        <?php foreach ( $services as $service ): ?>
            <div class="service-item grid">
                <div class="grid__item large--two-tenths medium--three-tenths">
                    <?php if ( $image = $service['image'] ) : ?>
                        <div class="service-item__image">
                            <?php echo wp_get_attachment_image( $image['id'], 'large' ); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="grid__item large--eight-tenths medium--seven-tenths">
                    <?php if ( $service['title'] ) : ?>
                        <h2 class="service-item__title"><?php echo $service['title']; ?></h2>
                    <?php endif; ?>

                    <div class="grid">
                        <?php if ( $service['text'] ) : ?>
                            <div class="grid__item large--one-half entry-content service-item__text">
                                <?php echo $service['text']; ?>
                            </div>
                        <?php endif; ?>

                        <?php if ( $service['capabilities'] ) : ?>
                            <div class="grid__item large--one-half entry-content service-item__caps">
                                <?php echo $service['capabilities']; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        
        <?php
        $button_text = get_field('button_text');
        $button_url = get_field('button_url');
        if ($button_text && $button_url) : ?>
            <div class="text-center">
                <a href="<?php echo $button_url; ?>" class="button button--primary button--outline font-weight-bold" target="_blank">
                    <?php echo $button_text; ?>    
                </a> 
            </div>
        <?php endif; ?>
    </div>
<?php endif; 