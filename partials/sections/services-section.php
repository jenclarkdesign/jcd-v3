<?php if ( $link_list = get_field('link_list') ) : ?>
    <div class="link-list-section">
        <div class="wrapper link-list-section__wrapper">
            <?php foreach ( $link_list as $item ) : ?>
                <div class="link-list-item">
                    <div class="link-list-item__inner clearfix">
                        <?php if ( $item['image'] ) : ?>
                            <div data-direction="down" data-timing="sineInOut" data-modifier="5" class="js-service-parallax link-list-item__image layer-cover-bg" style="background-image: url('<?php echo wp_get_attachment_image_url( $item['image']['id'], 'project-image' ); ?>')">
                                <?php echo wp_get_attachment_image( $item['image']['id'], 'project-image' ); ?>
                            </div>
                        <?php endif; ?>

                        <div data-direction="up" data-timing="backInOut" data-modifier="13" class="js-service-parallax link-list-item__content">
                            <?php if ( $item['title'] ) : ?>
                                <h3 class="link-list-item__title"><?php echo $item['title']; ?></h3>
                            <?php endif; ?>

                            <?php if ( $item['description'] ) : ?>
                                <div class="link-list-item__description"><?php echo $item['description']; ?></div>
                            <?php endif; ?>

                            <?php if ( $item['button_text'] && $item['button_url'] ) : ?>
                                <a class="link-list-item__link" href="<?php echo esc_url($item['button_url']); ?>">
                                    <?php echo $item['button_text']; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>