<?php
$hero_image       = get_field( 'hero_image' );
// $about_image      = get_field( 'about_image' );
$about_text       = get_field( 'about_text' );
$about_button_text= get_field( 'about_button_text' );
$about_button_link= get_field( 'about_button_link' );

if( $about_text ): ?>
    <section id="hero-section" class="hero-section">
        <div class="hero-section__wrapper">
            <div class="hero-section__content js-hero-parallax" data-direction="up" data-modifier="10">
                <?php if ($about_text) : ?>
                    <div class="hero-section__description"><?php echo $about_text; ?></div>
                <?php endif; ?>

                <?php if ($about_button_link && $about_button_text) : ?>
                    <a href="<?php echo $about_button_link; ?>" class="hero-section__link link-list-item__link">
                        <?php echo $about_button_text; ?>
                    </a>
                <?php endif; ?>
            </div>

            <?php if ($hero_image) : ?>
                <div class="hero-section__image">
                    <div class="hero-section__background layer-cover layer-cover-bg" style="background-image: url('<?php echo wp_get_attachment_image_url($hero_image['id'], 'hero-image'); ?>')"></div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif;