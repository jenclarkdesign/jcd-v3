<?php

$title = get_field( 'title_membership' );
$members = get_field( 'memberships', 'option' );

if( $members ): ?>
	<div class="membership-section block-section pb-0">
        <?php if ( $title ) : ?>
            <h3 class="block-section__title membership-section__title text-align-center"><?php echo $title; ?></h3>
        <?php endif; ?>

        <div class="membership-list">
            <?php foreach ($members as $member) : ?>
                <?php if ( $member['logo'] ) : ?>                    
                    <a href="<?php echo $member['link']; ?>" class="membership-item" target="_blank" rel="nofollow">
                        <?php echo wp_get_attachment_image($member['logo']['id'], 'member-thumbs'); ?>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif;
