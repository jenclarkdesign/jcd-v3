<?php if( $design_process = get_field( 'design_process' ) ): ?>
    <div class="step-tree-wrapper">
        <ul class="step-tree mb-60">
            <?php
            $counter = 1;
            foreach ( $design_process as $process ) : 
                if ( count($design_process) === $counter ) {
                    $extra_class = 'last';
                } else {
                    if ( $counter % 2 === 0 ) {
                        $extra_class = 'even';
                    } else {
                        $extra_class = 'odd';
                    }
                }
                ?>
                <li class="step-tree__item <?php echo $extra_class; ?>">
                    <div class="step-tree__item__counter">
                        <?php echo $counter; ?>
                    </div>
                    <div class="step-tree__item__content">
                        <h3 class="step-tree__item__title"><?php echo $process['title'];?></h3>
                        <div class="step-tree__item__description"><?php echo $process['description'];?></div>
                    </div>
                </li>
            <?php $counter++; endforeach; ?>
        </ul>
        
        <?php
        $button_text = get_field('button_text');
        $button_link = get_field('button_link');
        if ( $button_text && $button_link ) : ?>
            <div class="text-center">
                <a href="<?php echo $button_link; ?>" class="button button--primary button--outline font-weight-bold" target="_blank">
                    <?php echo $button_text; ?>    
                </a> 
            </div>
        <?php endif; ?>
    </div>
<?php endif; 