<?php
$image = get_sub_field('image');
$image_decoration = get_sub_field('image_decoration');
$image_decor_color = get_sub_field('image_decoration_color');
$align = get_sub_field('vertical_align');
$title = get_sub_field('title');
$text = get_sub_field('text');

$extra_classes = [];

if ( $image_decoration === true ) {
    $extra_classes[] = 'has-decoration';
}

$extra_classes[] = "vertical-align-{$align}";
?>

<div class="image-row-section <?php echo implode(' ', $extra_classes); ?>">
    <div class="grid grid--middle grid--rev">
        <div class="grid__item image-row-section__grid large--one-half medium--one-half">
            <?php if ( $image ) : ?>
                <div class="image-row-section__image">
                    <?php if ($image_decoration === true && $image_decor_color) : ?>
                        <div class="image-row-section__image-decor" style="background-color: <?php echo $image_decor_color; ?>"></div>
                    <?php endif; ?>
                    <?php echo wp_get_attachment_image( $image['id'], 'about-thumb' ); ?>
                </div>
            <?php endif; ?>
        </div><!--

     --><div class="grid__item image-row-section__grid large--one-half medium--one-half">
            <?php if ( $title ) : ?>
                <h2 class="image-row-section__title"><?php echo $title; ?></h2>
            <?php endif; ?>
            
            <?php if ($text) : ?>
                <div class="image-row-section__text">
                    <?php echo $text; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!-- /.image-row-section -->