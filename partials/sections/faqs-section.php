<?php if( $faqs = get_field( 'faqs' ) ): ?>
	<div class="faq-section accordion-section">
        <?php foreach ( $faqs as $faq ): ?>
            <?php if ( $faq['title'] && $faq['description'] ) : ?>
                <div class="accordion-item">
                    <button type="button" class="button--reset accordion-item__button"></button>
                    <div class="accordion-item__inner">
                        <h2 class="accordion-item__title"><?php echo $faq['title']; ?></h2>
                        <div class="accordion-item__description"><?php echo $faq['description']; ?></div>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>	
	</div>
<?php endif; 