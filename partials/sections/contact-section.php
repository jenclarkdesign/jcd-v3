<?php
$contact_title = get_field('footer_contact_form_title', 'option');
$contact_text = get_field('footer_contact_form_description', 'option');
$side_classes = 'grid__item large--three-twelfths push--large--one-twelfth';

if ( is_page_template( 'template-contact.php' ) ) {
    $side_classes = 'grid__item large--four-twelfths';
}

?>

<div class="contact-section block-section">
    <?php if ($contact_title && ! is_page_template('template-contact.php')) : ?>
        <h3 class="block-section__title contact-section__title"><?php echo $contact_title; ?></h3>
    <?php endif; ?>

    <div class="grid">
        <div class="grid__item large--eight-twelfths">
            <?php if ($contact_text) : ?>
                <div class="contact-section__text"><?php echo $contact_text; ?></div>
            <?php endif; ?>

            <!-- GForm Code -->
            <div class="contact-section__form">
                <?php gravity_form('1', $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true); ?>
            </div>
        </div>
        
        <div class="<?php echo $side_classes; ?>">
            <div class="contact-section__side">
                <?php 
                the_widget( 
                    'JCD_Mailchimp_Subscribe', 
                    array(
                        'title' => __('Subscribe to Our Newsletter', 'jcd'),
                        'form_action_url' => 'jcd@admin.com',
                    ),
                    array(
                        'before_title' => '<h3 class="widget__title">',
                        'after_title' => '</h3>',
                    )
                );
                ?>

                <aside class="widget widget-social-link">
                    <h3 class="widget__title"><?php _e('Follow us on:', 'jcd'); ?></h3>
                    <?php get_template_part('partials/component/social-links'); ?>
                </aside>
            </div>
        </div>
    </div>
</div>
