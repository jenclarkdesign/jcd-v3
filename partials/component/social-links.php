<div class="social-links">
    <?php 
    $social_links = array( 
        'facebook' => 'facebook',
        'twitter' => 'twitter',
        'instagram' => 'instagram',
        'googleplus' => 'google+',
        'linkedin' => 'linkedIn',
        'pinterest' => 'pinterest',
    );
    $collections = array(); 
    foreach( $social_links as $link => $icon ) : 
        if( $url = get_field('social_link_' . $link, 'options') ) : 
            $collections[] = '<a href="'.$url.'" target="_blank" class="'.$link.'"> ' . ucwords($icon). '</a>'; 
        endif;  
    endforeach;

    if( count( $collections ) > 0 ){
        $count = count( $collections ) - 1;
        foreach ( $collections as $i => $social ) {
            echo $social;
            if( $count != $i ){
                echo ' / ';
            }
        }
    }

    ?> 
</div>