<?php
$title = get_the_title();
$blog_id = get_option('page_for_posts');

if (is_404()) {
    $title = __('Page Not Found', 'jcd');
} else if ( is_home() ) {
    $title = get_the_title( $blog_id );
} else if ( is_post_type_archive( 'projects' ) ) {
    $title = __('Projects', 'jcd');
} else if ( is_archive() ) {
    $title = get_the_archive_title();
}
?>

<div class="page-heading">
    <div class="wrapper">
        <h1 class="page-heading__title entry-title text-center"><?php echo $title; ?></h1>

        <?php if ( is_singular('post') ) : ?>
            <div class="page-heading__meta">
                <?php printf(
                    __('Posted on %s <span class="page-heading__meta__sep">&bull;</span> By %s', 'jcd'),
                    '<time datetime="'. get_the_date(' c ') . '">' . get_the_date() . '</time>',
                    '<a href="'. get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')) . '">'. get_the_author() .'</a>'
                ); ?> <span class="page-heading__meta__sep">&bull;</span> In <?php the_category(' / '); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
