<div <?php post_class('portfolio-item grid__item large--four-twelfths medium--one-half js-box-item'); ?>>
    <a href="<?php the_permalink(); ?>" class="portfolio-item__inner" title="<?php the_title(); ?>">
        <?php if (has_post_thumbnail()) : ?>
            <div class="portfolio-item__image layer-cover layer-cover-bg" style="background-image: url('<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'project-image' ); ?>')"></div>
        <?php endif; ?>

        <div class="portfolio-item__meta layer-cover">
            <h3 class="portfolio-item__title"><?php the_title(); ?></h3>
            <!-- <span class="portfolio-item__button"><?php _e('View Project', 'jcd'); ?></span> -->
        </div>
    </a>
</div>