<?php
global $loop;

$post_classes = array( 'blog-item', 'grid__item', 'small-landscape--one-half', 'small--one-whole' );
$columns = 3;

if ( isset( $loop['columns'] ) ) {
    $columns = $loop['columns'];
}

switch ( $columns ) {
    case 2:
        $post_classes[] = 'large--one-half';
        $post_classes[] = 'medium--one-half';
        break;
    
    default:
        $post_classes[] = 'large--one-third';
        $post_classes[] = 'medium--one-third'; 
        break;
}

?>

<article <?php post_class( implode(' ', $post_classes) ); ?>>
    <?php 
    $thumb_id = '';
    if (has_post_thumbnail()) {
        $thumb_id = get_post_thumbnail_id();
    } elseif ($first_image = jcd_get_post_first_image(get_the_id())) {
        $thumb_id = $first_image;
    }
    
    if ($thumb_id) : ?>
        <div class="blog-item__image mb-30">
            <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>">
                <?php echo wp_get_attachment_image( $thumb_id, 'blog-thumb', '', array( 'class' => 'display-block' ) ); ?>
            </a>
        </div>
    <?php endif; ?>	

    <div class="blog-item__meta mb-20">
        <time datetime="<?php echo get_the_time('c'); ?>">
            <?php printf(__('Posted on %s', 'jcd'), get_the_time( 'j F Y' )); ?>
        </time>
        <div><?php printf(__('By %s', 'jcd'), get_the_author_link()); ?></div>
        <div>In <?php the_category(' / '); ?></div>
    </div>

    <h2 class="blog-item__title mb-20">
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </h2>

    <div class="blog-item__excerpt entry-content">
        <?php the_excerpt(); ?>
    </div>
</article>