const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

// Multiple CSS output
const extractApp = new ExtractTextPlugin('app.min.css');

module.exports = {
	entry: [
		'./src/js/main.js',
		'./src/sass/app.scss'
	],

	plugins: [
		extractApp,
		new UglifyJSPlugin()
	],

	output: {
		path: __dirname,
		filename: 'includes/js/scripts.min.js'
	},

	externals: {
		jquery: 'jQuery',
		underscore: '_',
		backbone: 'Backbone',
		wp: 'wp'
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ["babel-loader"]
			},
			{
				test: /app\.scss$/,
				use: extractApp.extract({
					fallback: "style-loader",
					use: [
						{ 
							loader: "css-loader",
							options: {
								minimize: false,
								importLoaders: 1
							}
						},
						{ loader: "resolve-url-loader" },
						{ loader: "postcss-loader" },
						{
							loader: "sass-loader",
							options: { }
						}
					]
				})
			},
			{
				test: /\.(png|jpg|jpeg)(\?[0-9].+)?$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'file-loader',
						options: {
							limit: '10000',
							name: "images/[name].[ext]"
						}
					}
				]
			},
			{
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				use: [
					{
						loader: "file-loader",
						options: {
							limit: '10000',
							mimetype: "application/font-woff",
							name: "fonts/[name].[ext]"
						}
					}
				]
			},
			{
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "fonts/[name].[ext]"
						}
					}
				]
			},
			{
				test: /(default-skin|preloader)\.(png|svg|gif)(\?[0-9].+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							limit: '10000',
							name: "images/[name].[ext]"
						}
					}
				]
			}
		]
	},

	stats: {
		// Colored output
		colors: true
	}
};
