<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head <?php language_attributes(); ?>>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <?php jcd_head(); ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class('antialiased'); ?>>
    
    <header class="sticky-bar">
        <div class="sticky-bar__inner">
            <div class="topbar-section">
                <div class="wrapper wrapper--full topbar-section__wrapper">

                    <div class="grid grid--middle topbar-section-grid">
                        <div class="grid__item six-twelfths large--two-twelfths">
                            <div class="logo">
                                <a class="logo__link" href="<?php echo home_url(); ?>" title="<?php echo get_bloginfo('description'); ?>">
                                    <?php if ($logo = get_field('logo', 'option')) : ?>
                                        <img src="<?php echo esc_url($logo['url']); ?>" alt="<?php esc_attr_e(get_bloginfo('site_title')); ?>" class="logo__img logo-main">
                                    <?php else : ?>
                                        <?php echo get_bloginfo('site_title'); ?>
                                    <?php endif; ?>

                                    <?php if ( $logo_alt = get_field('alternative_logo', 'option') ) : ?>
                                        <img src="<?php echo esc_url($logo_alt['url']); ?>" alt="<?php esc_attr_e(get_bloginfo('site_title')); ?>" class="logo__img logo-alt">
                                    <?php endif; ?>
                                </a>
                            </div>
                        </div><!--

                     --><div class="grid__item six-twelfths large--ten-twelfths text-align-right mobile-nav-grid">
                            <?php if ($phone_number = get_field('phone', 'option')) : ?>
                                <div class="main-nav-phone">
                                    <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a>
                                </div>
                            <?php endif; ?>

                            <button class="menu-trigger">
                                <span class="menu-trigger__bar"></span>
                                <span class="menu-trigger__bar"></span>
                                <span class="menu-trigger__bar"></span>
                            </button>
                        </div><!--

                     --><div class="grid__item large--ten-twelfths main-nav-grid">
                            <div class="main-nav-flex">
                                <div class="main-nav-scroller">
                                    <nav class="main-nav js-horz-container clearfix text-align-right">
                                        <ul class="js-horz-content">
                                            <?php
                                                wp_nav_menu(array(
                                                    'theme_location' => 'main-menu',
                                                    'container' => '',
                                                    'container_class' => '',
                                                    'menu_class' => '',
                                                    'items_wrap' => '%3$s',
                                                    'fallback_cb' => false,
                                                    'item_spacing' => 'discard',
                                                    'depth' => 1,
                                                ));
                                            ?>
                                        </ul>
                                    </nav><!-- .main-nav -->

                                    <button class="horizontal-nav__button js-horz-left-button button--reset prev">
                                        <i class="icon-chevron-left"></i>
                                    </button>

                                    <button class="horizontal-nav__button js-horz-right-button button--reset next">
                                        <i class="icon-chevron-right"></i>
                                    </button>
                                </div>

                                <?php if ( $phone_number = get_field('phone', 'option') ) : ?>
                                    <div class="main-nav-phone">
                                        <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
    <!-- .sticky-bar -->

    <div class="outer-content-wrapper">
