<?php
/**
 * Template Name: Contact
 */

get_header( ); ?>

<?php get_template_part( 'partials/component/page', 'heading' ); ?>

<div class="main-content-section wrapper block-section">
    <?php while( have_posts() ) : the_post(); ?>
        <article <?php post_class(); ?>>
            <?php get_template_part('partials/sections/contact-section'); ?>
        </article>
    <?php endwhile; ?>
</div><!-- .main-content-section -->

<?php get_footer();