<?php get_header();?>

<?php get_template_part( 'partials/component/page', 'heading' ); ?>

<div class="main-content-section block-section wrapper">
    <?php 
    $archive_description = get_field('projects_page_description', 'option');
    global $post; ?>
    <div class="page-description text-align-center">
        <?php if ( $post->post_content ) : ?>
            <?php the_content(); ?>
        <?php else : ?>
            <?php echo $archive_description; ?>
        <?php endif; ?>
    </div>

    <div class="grid">

        <div class="grid__item main-content large--ten-twelfths push--large--one-twelfth">
            <?php while( have_posts() ) : the_post(); ?>
                <article <?php post_class(); ?>>
                    <!-- <div class="entry-content"><?php the_content(); ?></div> -->
                    <?php
                        $images = get_field('project_gallery'); 
                        $gallery_first = array();
                        $gallery_ids = array();
                        if( $images ) : ?>
                            <div class="project-images grid" data-pswp-uid="<?php echo get_the_id(); ?>">
                                <?php
                                $counter = 1;
                                $index = 0;
                                foreach ($images as $image) : 
                                    $image_size = 'project-gallery-image';
                                    $image_class = 'project-images__item--wide';

                                    // if ( $counter === 2 || $counter === 3 ) {
                                    //     $image_size = 'project-image';
                                    //     $image_class = 'project-images__item--half large--one-half';
                                    // }
                                    
                                    // If the last image is half wide but doesn't have image next to it
                                    // if ( count($images) - 1 === $index && $counter === 2 ) {
                                    //     $image_size = 'project-gallery-image';
                                    //     $image_class = 'project-images__item--wide';
                                    // }

                                    ?>
                                    <a class="project-images__item grid__item <?php echo $image_class; ?>"
                                        href="<?php echo $image['url']; ?>" 
                                        title="<?php _e('Show full image size', 'jcd'); ?>" 
                                        data-index="<?php echo $index; ?>"
                                        data-width="<?php echo $image['width']; ?>"
                                        data-height="<?php echo $image['height']; ?>"
                                    >
                                        <?php echo wp_get_attachment_image( $image['id'], $image_size ); ?>
                                    </a>
                                <?php 
                                $counter++;
                                $index++;
                                if ( $counter === 4 ) {
                                    $counter = 1;
                                }
                                endforeach; ?>
                            </div>
                    <?php endif; ?>
                </article>
            <?php endwhile; ?>
        </div>
    </div>
</div><!-- .main-content-section -->

<?php	get_footer(); ?>
