<?php
/**
 * Template Name: Services & Fees
 */
get_header( );

if ( have_posts()) : while (have_posts() ) : the_post();

    get_template_part( 'partials/component/page', 'heading' ); ?>

    <div class="main-content-section block-section wrapper">
        <div class="page-description text-align-center">
            <?php the_content(); ?>
        </div>
        
        <div class="grid">
            <div class="grid__item large--ten-twelfths push--large--one-twelfth">
                <?php get_template_part('partials/sections/services-details'); ?>
            </div>
        </div>
    </div>

<?php endwhile; endif;

get_footer();