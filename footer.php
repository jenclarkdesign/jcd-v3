        <footer class="footer-section">
            <div class="wrapper">

                <?php 
                if ( ! is_page_template( 'template-contact.php' ) ) {
                    get_template_part( 'partials/sections/contact-section' );
                }
                ?>
                
                <div class="footer-nav-scroller">
                    <nav class="footer-nav js-horz-container main-nav">
                        <ul class="js-horz-content">
                            <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'footer-menu',
                                    'container' => '',
                                    'container_class' => '',
                                    'menu_class' => '',
                                    'items_wrap' => '%3$s',
                                    'fallback_cb' => false,
                                    'item_spacing' => 'discard',
                                    'depth' => 1,
                                ));
                            ?>
                        </ul>
                    </nav>

                    <button class="horizontal-nav__button js-horz-left-button button--reset prev">
                        <i class="icon-chevron-left"></i>
                    </button>

                    <button class="horizontal-nav__button js-horz-right-button button--reset next">
                        <i class="icon-chevron-right"></i>
                    </button>
                </div>

                <div class="copyrights"><?php jcd_copyrights(); ?></div>

            </div>
        </footer>
        <!-- /.footer-section -->
    
    </div>
    <!-- /.outer-content-wrapper -->

    <?php 
    if ( is_singular( 'projects' ) ) {
        get_template_part( 'partials/component/project-image-modal' );
    }
    ?>


    <div id="right-panel" class="side-panel">
        <div class="mm-panels">
            <ul>
                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'main-menu',
                        'container' => '',
                        'container_class' => '',
                        'menu_class' => '',
                        'items_wrap' => '%3$s',
                        'fallback_cb' => false,
                        'item_spacing' => 'discard',
                        'depth' => 1,
                    ));
                ?>
            </ul>
        </div>
    </div>

    <?php wp_footer(); ?>

</body>
</html>
