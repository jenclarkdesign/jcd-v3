<?php
get_header( );

get_template_part( 'partials/component/page', 'heading' ); ?>

<div class="main-content-section block-section wrapper">
    <?php if ( $archive_description = get_field('projects_page_description', 'option') ) : ?>
        <div class="page-description text-align-center">
            <?php echo $archive_description; ?>
        </div>
    <?php endif; ?>

    <div class="grid portfolio-list js-mosaic-layout">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part('partials/content/portfolio-item'); ?>
        <?php endwhile; ?>
    </div>
</div>
        
<?php get_footer();