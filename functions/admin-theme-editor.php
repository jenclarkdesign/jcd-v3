<?php
/**
 * Add Syntax Highlighting for Theme Editor page
 *
 * @author	Arif Widi @mambows
 * @copyright	Copyright ( c ) Arif Widi
 * @link	http://jcd.me
 * @since	Version 1.2
 * @package 	JcdFramework
 */

function jcd_editor_scripts() {

	if( isset( $_REQUEST['file'] ) ) {
		$file = $_REQUEST['file'];
	} else {
		$file = 'style.css';
	}
	$file_ext = substr( strchr( $file, '.' ), 1 );
	$mime_type = 'text/css';
	if( 'php' == $file_ext ) {
		$mime_type = 'application/x-httpd-php';
	}

	wp_enqueue_script( 'jcd-codemirror', get_template_directory_uri() . '/functions/js/codemirror/codemirror-compressed.js', array('jquery') );
	wp_enqueue_script( 'jcd-theme-editor', get_template_directory_uri() . '/functions/js/jcd-theme-editor.js', array('jcd-codemirror') );

	wp_enqueue_style( 'jcd-codemirror', get_template_directory_uri() . '/functions/js/codemirror/codemirror.css' );
	wp_enqueue_style( 'jcd-theme-editor', get_template_directory_uri() . '/functions/css/jcd-theme-editor.css' );

	wp_localize_script( 'jcd-theme-editor', 'jcd_editor_mime', $mime_type );
}
add_action( 'admin_print_scripts-theme-editor.php', 'jcd_editor_scripts' );