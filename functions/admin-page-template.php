<?php
/**
 * Page setting for re-arrange sections
 *
 * @package WordPress
 * @subpackage Shoppinggo
 * @since Shoppinggo 1.0
 */

add_action( 'admin_enqueue_scripts', 'jcd_ptemplate_scripts' );
function jcd_ptemplate_scripts( $hook_suffix ) {
	if ( in_array($hook_suffix, array('post.php','post-new.php')) ) {

        // enqueue script js
		wp_register_script('et-ptemplates', get_template_directory_uri() . '/functions/js/jcd-page-templates.js', array('jquery'));
		wp_register_script('jcd-widgets', get_template_directory_uri() . '/functions/js/jcd-widgets.js', array( 'jquery', 'jquery-ui-sortable', 'jquery-ui-draggable', 'jquery-ui-droppable' ), false, true );
		wp_enqueue_script('et-ptemplates');
		wp_enqueue_script('jcd-widgets');

        // enqueue style css
        wp_enqueue_style('jcd-widgets-style', get_stylesheet_directory_uri().'/functions/css/jcd-homepage-builder.css');
	}
}

add_action("admin_init", "jcd_ptemplates_metabox");
function jcd_ptemplates_metabox(){
	add_meta_box("jcd_ptemplate_meta", "Jcd Homepage Builder", "jcd_ptemplate_meta", "page", "normal", "high");
}

if ( ! function_exists( 'jcd_ptemplate_meta' ) ){
	function jcd_ptemplate_meta($callback_args) {
		global $post, $jcd_options_homepage;

                // initial counter
                $i  = 1;

                // check if option is empty
                if( empty($jcd_options_homepage) ) return;
                ?>

            <div id="available-widgets">
                <p class="description">Inactive Elements for Homepage</p>
                <div id="widget-list">
                    <?php foreach( $jcd_options_homepage as $id => $item ){ ?>

                        <?php jcd_homepage_widget_list($post->ID, $id, $item, $i); ?>

                    <?php $i++; } ?>
                </div>
            </div>


            <div class="widgets-sortables">
                <p class="description">Active Elements for Homepage (Drag element from above)</p>
                <?php jcd_homepage_widget_assigned($post->ID); ?>
            </div>

		<?php
	}
}

function jcd_homepage_widget_list($post_id, $id, $item, $i){
    ?>
                        <div id="widget-<?php echo $i; ?>_<?php echo $id; ?>-__i__" class="widget">
                            <div class="widget-top">
                                <div class="widget-title-action">
                                        <a class="widget-action hide-if-no-js" href="#available-widgets"></a>
                                        <a class="widget-control-edit hide-if-js" href="#">
                                                <span class="edit">Edit</span>
                                                <span class="add">Add</span>
                                                <span class="screen-reader-text"><?php echo $item['title']; ?></span>
                                        </a>
                                </div>
                                <div class="widget-title"><h4><?php echo $item['title']; ?><span class="in-widget-title"></span></h4></div>
                            </div>

                            <div class="widget-inside">
                                <div class="widget-content">
                                    <?php if( isset( $item['desc'] ) ) : ?>
                                        <p class="block-desc"><?php echo $item['desc'];?></p>
                                    <?php endif; ?>
                                    <?php jcd_homepage_widget_content($post_id, $item['options']); ?>
                                </div>
                                <input type="hidden" name="<?php echo 'meta-widget[__i__][widget_id]'; ?>" class="widget-id" value="<?php echo $id; ?>-__i__">
                                <input type="hidden" name="<?php echo 'meta-widget[__i__][id_base]'; ?>" class="id_base" value="<?php echo $id; ?>">
                                <input type="hidden" name="widget-width" class="widget-width" value="100">
                                <input type="hidden" name="widget-height" class="widget-height" value="200">
                                <input type="hidden" name="<?php echo 'meta-widget[__i__][widget_number]'; ?>" class="widget_number" value="<?php echo $i; ?>">
                                <input type="hidden" name="<?php echo 'meta-widget[__i__][title]'; ?>" class="widget_title" value="<?php echo $item['title']; ?>">

                                <div class="widget-control-actions">
                                    <div class="alignleft">
                                        <a class="widget-control-remove" href="#remove">Delete</a> |
                                        <a class="widget-control-close" href="#close">Close</a>
                                    </div>
                                    <br class="clear">
                                </div>
                            </div>
                            <div class="widget-description"><?php $item['desc']; ?></div>
                        </div>

    <?php
}

function jcd_homepage_widget_content($post_id, $options){
        global $wp_registered_sidebars;

        $output = '';
        $counter = 1;

        // generate the front-end
        foreach( $options as $key => $subitem ){

            $classname = ( $counter % 2 == 0 ) ? 'form-input-right' : 'form-input-left';
            $default = ( isset( $subitem['default'] ) ) ? $subitem['default'] : '';
            $form_input_open = '<p class="'. $classname .' form-input-wrapper form-input-wrapper-'. $subitem['type'] .'">';
            $form_input_close= '</p>';

            $output .= $form_input_open;
            switch ($subitem['type']) {

                case 'info':
                        $output .= $subitem['std'];
                    break;
                case 'text':
                        $output .= '<label for="meta-widget[__i__][options]['.$key.']">' . $subitem['label'] . ':</label> <input id="meta-widget[__i__][options]['.$key.']" name="meta-widget[__i__][options]['.$key.']" type="text" class="jcd-input-text" value="'. $default .'">';
                    break;
                case 'textarea':
                        $output .= '<label for="meta-widget[__i__][options]['.$key.']">' . $subitem['label'] . ':</label> <textarea id="meta-widget[__i__][options]['.$key.']" name="meta-widget[__i__][options]['.$key.']" class="jcd-input-textarea">'. $default .'</textarea>';
                    break;
                case 'checkbox':
                        $output .= '<label for="meta-widget[__i__][options]['.$key.']">' . $subitem['label'] . ':</label> <input id="meta-widget[__i__][options]['.$key.']" name="meta-widget[__i__][options]['.$key.']" type="checkbox" class="jcd-input-checkbox">';
                    break;
                case "color":
                        $output .= '<div id="' . $value['id'] . '_picker" class="colorSelector"><div style="background-color: '. esc_attr( $val ) .'"></div></div>';
                        $output .= '<input class="jcd-color" name="'. $value['id'] .'" id="'. $value['id'] .'" type="text" value="'. esc_attr( $val ) .'" />';
                    break;
                case 'widget':
                        $output .= '<label for="meta-widget[__i__][options]['.$key.']">' . $subitem['label'] . ':</label>';
                        $output .= '<select id="meta-widget[__i__][options]['.$key.']" name="meta-widget[__i__][options]['.$key.']">';
                        foreach( $wp_registered_sidebars as $value => $item ){
                            $output .= '<option value="'.$value.'">'.$item['name'].'</option>';
                        }
                        $output .= '</select>';
                    break;
                case 'select':
                        $output .= '<label for="meta-widget[__i__][options]['.$key.']">' . $subitem['label'] . ':</label>';
                        $output .= '<select id="meta-widget[__i__][options]['.$key.']" name="meta-widget[__i__][options]['.$key.']">';
                        foreach( $subitem['options'] as $value => $item ){
                            $selected = ( $value == $default ) ? 'selected' : '';
                            $output .= '<option value="'.$value.'" '. $selected .'>'.$item.'</option>';
                        }
                        $output .= '</select>';
                    break;
                default:
                    break;
            }

            // Description
            if( isset( $subitem['desc'] ) ) {
                $output .= '<span class="form-desc">'. $subitem['desc'] .'</span>';
            }
            $output .= $form_input_close;

            // Clear elements
            if( $counter == 2 || $subitem['type'] == 'textarea' ) {
                $output .= '<div class="clear"></div>';
                $counter = 0;
            }

            $counter++;
        }

        echo $output;
}

function jcd_homepage_widget_assigned($post_id){
        global $jcd_options_homepage;

        // get meta data
        $meta_widget = get_post_meta($post_id, '_meta-widget', true );

        // Check if data is serialized
        if( is_serialized( $meta_widget ) ) {
            $meta_widget = unserialize( $meta_widget );
        }
        $i  = 1;

        // check if option is empty
        if( empty($jcd_options_homepage) || empty($meta_widget)) return;

        foreach( $meta_widget as $key => $item ){
            if($key == '__i__') continue;
            ?>
                        <div id="widget-<?php echo $item['widget_number']; ?>_<?php echo $item['id_base']; ?>-<?php echo $i; ?>" class="widget">
                            <div class="widget-top">
                                <div class="widget-title-action">
                                        <a class="widget-action hide-if-no-js" href="#available-widgets"></a>
                                        <a class="widget-control-edit hide-if-js" href="#">
                                                <span class="edit">Edit</span>
                                                <span class="add">Add</span>
                                                <span class="screen-reader-text"><?php echo $item['title']; ?></span>
                                        </a>
                                </div>
                                <div class="widget-title"><h4><?php echo $item['title']; ?><span class="in-widget-title"></span></h4></div>
                            </div>

                            <div class="widget-inside">
                                <div class="widget-content">
                                    <?php $desc = $jcd_options_homepage[$item['id_base']]['desc']; ?>
                                    <?php if( isset( $desc ) ) : ?>
                                        <p class="block-desc"><?php echo $desc;?></p>
                                    <?php endif; ?>
                                    <?php $item['options'] = !isset($item['options']) ? array() : $item['options']; ?>
                                    <?php jcd_homepage_widget_content_assigned($i, $item['id_base'], $item['options']); ?>
                                </div>
                                <input type="hidden" name="<?php echo 'meta-widget['.$i.'][widget_id]'; ?>" class="widget-id" value="<?php echo $item['id_base']; ?>-<?php echo $i; ?>">
                                <input type="hidden" name="<?php echo 'meta-widget['.$i.'][id_base]'; ?>" class="id_base" value="<?php echo $item['id_base']; ?>">
                                <input type="hidden" name="widget-width" class="widget-width" value="100">
                                <input type="hidden" name="widget-height" class="widget-height" value="200">
                                <input type="hidden" name="<?php echo 'meta-widget['.$i.'][widget_number]'; ?>" class="widget_number" value="<?php echo $item['widget_number']; ?>">
                                <input type="hidden" name="<?php echo 'meta-widget['.$i.'][title]'; ?>" class="widget_title" value="<?php echo $item['title']; ?>">

                                <div class="widget-control-actions">
                                    <div class="alignleft">
                                        <a class="widget-control-remove" href="#remove">Delete</a> |
                                        <a class="widget-control-close" href="#close">Close</a>
                                    </div>
                                    <br class="clear">
                                </div>
                            </div>
                            <div class="widget-description"><?php $item['desc']; ?></div>
                        </div>

            <?php
            $i++;
        }
//        exit;
}

function jcd_homepage_widget_content_assigned( $index,  $id_base, $options = array() ){
        global $wp_registered_sidebars;

        $jcd_options_hompage = get_option( 'jcd_options_homepage', array());

        $output = '';
        $counter = 1;

        // generate the front-end
        foreach( $options as $key => $value ){

            $item   = $jcd_options_hompage[$id_base]['options'][$key];
            $classname = ( $counter % 2 == 0 ) ? 'form-input-right' : 'form-input-left';
            $form_input_open = '<p class="'. $classname .' form-input-wrapper form-input-wrapper-'. $item['type'] .'">';
            $form_input_close= '</p>';

            $output .= $form_input_open;
            switch ($item['type']) {

                case 'info':
                        $output .= $item['std'];
                    break;
                case 'text':
                        $output .= '<label for="meta-widget['.$index.'][options]['.$key.']">' . $item['label'] . ':</label> <input id="meta-widget['.$index.'][options]['.$key.']" name="meta-widget['.$index.'][options]['.$key.']" type="text" class="jcd-input-text" value="'.$value.'">';
                    break;
                case 'textarea':
                        $output .= '<label for="meta-widget['.$index.'][options]['.$key.']">' . $item['label'] . ':</label> <textarea id="meta-widget['.$index.'][options]['.$key.']" name="meta-widget['.$index.'][options]['.$key.']" class="jcd-input-textarea">'.$value.'</textarea>';
                    break;
                case 'widget':
                        $output .= '<label for="meta-widget['.$index.'][options]['.$key.']">' . $item['label'] . ':</label>';
                        $output .= '<select id="meta-widget['.$index.'][options]['.$key.']" name="meta-widget['.$index.'][options]['.$key.']">';
                        foreach( $wp_registered_sidebars as $key => $subitem ){
                            $selected = ($value == $key)? ' selected':'';
                            $output .= '<option value="'.$key.'"'.$selected.'>'.$subitem['name'].'</option>';
                        }
                        $output .= '</select>';
                    break;
                case 'select':
                        $output .= '<label for="meta-widget['.$index.'][options]['.$key.']">' . $item['label'] . ':</label>';
                        $output .= '<select id="meta-widget['.$index.'][options]['.$key.']" name="meta-widget['.$index.'][options]['.$key.']">';
                        foreach( $item['options'] as $key => $subitem ){
                            $selected = ($value == $key)? ' selected':'';
                            $output .= '<option value="'.$key.'"'.$selected.'>'.$subitem.'</option>';
                        }
                        $output .= '</select>';
                    break;
                default:
                    break;
            }

            // Description
            if( isset( $item['desc'] ) ) {
                $output .= '<span class="form-desc">'. $item['desc'] .'</span>';
            }

            $output .= $form_input_close;

            // Clear elements
            if( $counter == 2 || $item['type'] == 'textarea' ) {
                $output .= '<div class="clear"></div>';
                $counter = 0;
            }

            $counter++;
        }

        echo $output;
}

add_action('save_post', 'jcd_ptemplate_save_details');
function jcd_ptemplate_save_details( $post_id ){
	global $pagenow;

	if ( 'post.php' != $pagenow ) return $post_id;

	$post_info = get_post( $post_id );
	if ( 'page' != $post_info->post_type )
		return $post_id;

	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return $post_id;

	if ( !isset( $_POST["page_template"] ) )
		return $post_id;

	if ( !in_array( $_POST["page_template"], array('template-homepage.php') ) )
		return $post_id;

	if(!empty($_POST['meta-widget'])) {

        $meta_widget = $_POST['meta-widget'];
        /*foreach( $meta_widget as $key => &$item ){
            if($key == '__i__') continue;

            switch ( $item['id_base'] ) {
                case 'text':
                    $item['options']['text'] = $item['options']['text'];
                    break;
            }
        }*/

        update_post_meta($post_id, '_meta-widget', $meta_widget );
    }
}
?>
