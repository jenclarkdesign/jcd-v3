/**
 * This file holds the main javascript functions needed to improve the jcd mega menu backend
 *
 * @author	Onnay Okheng
 * @copyright	Copyright ( c ) Onnay Okheng
 * @link	http://onnayokheng.com
 * @link	http://jcd.me
 * @since	Version 1.0
 * @package 	JcdFramework
 */

(function($)
{
	var jcd_mega_menu = {
	
		recalcTimeout: false,
	
		// bind the click event to all elements with the class jcd_uploader 
		bind_click: function()
		{
			var megmenuActivator = $('.menu-item-jcd-megamenu', '#menu-to-edit');
				
				megmenuActivator.live('click', function()
				{	
					var checkbox = $(this),
						container = checkbox.parents('.menu-item:eq(0)');
				
					if(checkbox.is(':checked'))
					{
						container.addClass('jcd_mega_active');
					}
					else
					{
						container.removeClass('jcd_mega_active');
					}
					
					//check if anything in the dom needs to be changed to reflect the (de)activation of the mega menu
					jcd_mega_menu.recalc();
					
				});
		},
		
		recalcInit: function()
		{
			$( ".menu-item-bar" ).live( "mouseup", function(event, ui) 
			{
				if(!$(event.target).is('a'))
				{
					clearTimeout(jcd_mega_menu.recalcTimeout);
					jcd_mega_menu.recalcTimeout = setTimeout(jcd_mega_menu.recalc, 500);  
				}
			});
		},
		
		
		recalc : function()
		{
			menuItems = $('.menu-item', '#menu-to-edit');
			
			menuItems.each(function(i)
			{
				var item = $(this),
					megaMenuCheckbox = $('.menu-item-jcd-megamenu', this);
				
				if(!item.is('.menu-item-depth-0'))
				{
					var checkItem = menuItems.filter(':eq('+(i-1)+')');
					if(checkItem.is('.jcd_mega_active'))
					{
						item.addClass('jcd_mega_active');
						megaMenuCheckbox.attr('checked','checked');
					}
					else
					{
						item.removeClass('jcd_mega_active');
						megaMenuCheckbox.attr('checked','');
					}
				}				
				
				
				
				
				
			});
			
		},
		
		//clone of the jqery menu-item function that calls a different ajax admin action so we can insert our own walker
		addItemToMenu : function(menuItem, processMethod, callback) {
			var menu = $('#menu').val(),
				nonce = $('#menu-settings-column-nonce').val();

			processMethod = processMethod || function(){};
			callback = callback || function(){};

			params = {
				'action': 'jcd_ajax_switch_menu_walker',
				'menu': menu,
				'menu-settings-column-nonce': nonce,
				'menu-item': menuItem
			};

			$.post( ajaxurl, params, function(menuMarkup) {
				var ins = $('#menu-instructions');
				processMethod(menuMarkup, params);
				if( ! ins.hasClass('menu-instructions-inactive') && ins.siblings().length )
					ins.addClass('menu-instructions-inactive');
				callback();
			});
		}

};
	

	
	$(function()
	{
		jcd_mega_menu.bind_click();
		jcd_mega_menu.recalcInit();
		jcd_mega_menu.recalc();
		if(typeof wpNavMenu != 'undefined'){ wpNavMenu.addItemToMenu = jcd_mega_menu.addItemToMenu; }
 	});

	
})(jQuery);	 