if (!window.JcdFB) {
    window.JcdFB = {};
}

JcdFB_i18n = jQuery.extend( {
	nameLabel: 'Name',
	emailLabel: 'Email',
	urlLabel: 'Website',
	commentLabel: 'Comment',
	newLabel: 'New Field',
	optionsLabel: 'Options',
	optionLabel: 'Option',
	firstOptionLabel: 'First option',
	problemGeneratingForm: "Oops, there was a problem generating your form.  You'll likely need to try again.",
	moveInstructions: "Drag up or down\nto re-arrange",
	moveLabel: 'move',
	editLabel: 'edit',
	savedMessage: 'Saved successfully',
	requiredLabel: '(required)',
	exitConfirmMessage: 'Are you sure you want to exit the form editor without saving?  Any changes you have made will be lost.',
}, JcdFB_i18n );

JcdFB_i18n.moveInstructions = JcdFB_i18n.moveInstructions.replace( "\n", '<br />' );

JcdFB.span = jQuery( '<span>' );
JcdFB.esc_html = function( string ) {
	return JcdFB.span.text( string ).html();
};

JcdFB.esc_attr = function( string ) {
	string = JcdFB.esc_html( string );
	return string.replace( '"', '&quot;' ).replace( "'", '&#039;' );
};

JcdFB.ContactForm = function() {
	var fbForm = { // Main object that generated shortcode via AJAX call
	'action' : 'jcd_formbuilder_shortcode',
	'_ajax_nonce' : ajax_nonce_shortcode,
	'to' : '',
	'subject' : '',
	'fields' : {}
	};
	var defaultFields = {
		'name': {
			'label' : JcdFB_i18n.nameLabel,
			'type' : 'name',
			'required' : true,
			'options' : [],
			'order' : '1'
		},
		'email': {
			'label' : JcdFB_i18n.emailLabel,
			'type' : 'email',
			'required' : true,
			'options' : [],
			'order' : '2'
		},
		'url': {
			'label' : JcdFB_i18n.subjectLabel,
			'type' : 'text',
			'required' : false,
			'options' : [],
			'order' : '3'
		},
		'comment': {
			'label' : JcdFB_i18n.commentLabel,
			'type' : 'textarea',
			'required' : true,
			'options' : [],
			'order' : '4'
		}
	};
	var debug = true; // will print errors to log if true
	var grunionNewCount = 0; // increment for new fields
	var maxNewFields = 5;  // Limits number of new fields available
	var optionsCache = {};
	var optionsCount = 0; // increment for options
	var shortcode;

	function addField () {
		try {
			grunionNewCount++;
			if (grunionNewCount <= maxNewFields) {
				// Add to preview
				jQuery('#jcd-fb-extra-fields').append('<div id="jcd-fb-new-field' + grunionNewCount + '" fieldid="' + grunionNewCount + '" class="jcd-fb-new-fields"><div class="jcd-fb-fields"><div id="' + grunionNewCount + '" class="jcd-fb-remove"><span></span></div><label fieldid="' + grunionNewCount + '" for="jcd-fb-field' + grunionNewCount + '"><span class="label-text">' + JcdFB_i18n.newLabel + '</span> </label><input type="text" id="jcd-fb-field' + grunionNewCount + '" disabled="disabled" /></div></div>');
				// Add to form object
				fbForm.fields[grunionNewCount] = {
					'label' : JcdFB_i18n.newLabel,
					'type' : 'text',
					'required' : false,
					'options' : [],
					'order' : '5'
				};
				if (grunionNewCount === maxNewFields) {
					jQuery('#jcd-fb-new-field').hide();
				}
				// Reset form for this new field
				optionsCount = 0;
				optionsCache = {};
				jQuery('#jcd-fb-new-options').html('<label for="jcd-fb-option0">' + JcdFB_i18n.optionsLabel + '</label><input type="text" id="jcd-fb-option0" optionid="0" value="' + JcdFB_i18n.firstOptionLabel + '" class="jcd-fb-options" />');
				jQuery('#jcd-fb-options').hide();
				jQuery('#jcd-fb-new-label').val( JcdFB_i18n.newLabel );
				jQuery('#jcd-fb-new-type').val('text');
				jQuery('#jcd-fb-field-id').val(grunionNewCount);
				setTimeout(function () { jQuery('#jcd-fb-new-label').focus().select(); }, 100);
			} else {
				jQuery('#jcd-fb-new-field').hide();
			}
		} catch(e) {
			if (debug) {
				console.log("addField(): " + e);
			}
		}
	}
	function addOption () {
		try {
			optionsCount++;
			var thisId = jQuery('#jcd-fb-field-id').val();
			var thisType = jQuery('#jcd-fb-new-type').val();
			if (thisType === "radio") {
				// Add to right col
				jQuery('#jcd-fb-new-options').append('<div id="jcd-fb-option-box-' + optionsCount + '" class="jcd-fb-new-fields"><span optionid="' + optionsCount + '" class="jcd-fb-remove-option"></span><label></label><input type="text" id="jcd-fb-option' + optionsCount + '" optionid="' + optionsCount + '" value="' + JcdFB_i18n.optionLabel + '" class="jcd-fb-options" /><div>');
				// Add to preview
				jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').append('<div id="jcd-fb-radio-' + thisId + '-' + optionsCount + '"><input type="radio" disabled="disabled" id="jcd-fb-field' + thisId + '" name="radio-' + thisId + '" /><span>' + JcdFB_i18n.optionLabel + '</span><div class="clear"></div></div>');
			} else {
				// Add to right col
				jQuery('#jcd-fb-new-options').append('<div id="jcd-fb-option-box-' + optionsCount + '" class="jcd-fb-new-fields"><span optionid="' + optionsCount + '" class="jcd-fb-remove-option"></span><label></label><input type="text" id="jcd-fb-option' + optionsCount + '" optionid="' + optionsCount + '" value="" class="jcd-fb-options" /><div>');
				// Add to preview
				jQuery('#jcd-fb-field'+ thisId).append('<option id="jcd-fb-' + thisId + '-' + optionsCount + '" value="' + thisId + '-' + optionsCount + '"></option>');
			}
			// Add to fbForm object
			fbForm.fields[thisId].options[optionsCount] = "";
			// Add focus to new field
			jQuery('#jcd-fb-option' + optionsCount).focus().select();
		} catch(e) {
			if (debug) {
				console.log("addOption(): " + e);
			}
		}
	}
	function buildPreview () {
		try {
			if (fbForm.to) { jQuery('#jcd-fb-field-my-email').val(fbForm.to); }
			if (fbForm.subject) { jQuery('#jcd-fb-field-subject').val(fbForm.subject); }
			// Loop over and add fields
			jQuery.each(fbForm.fields, function(index, value) {
				jQuery('#jcd-fb-extra-fields').before('<div class="jcd-fb-new-fields ui-state-default" fieldid="' + index + '" id="jcd-fb-new-field' + index + '"><div class="jcd-fb-fields"></div></div>');
				jQuery('#jcd-fb-field-id').val(index);
				optionsCache[index] = {};
				optionsCache[index].options = [];
				if (value.type === "radio" || value.type === "select") {
					jQuery.each(value.options, function(i, value) {
						optionsCache[index].options[i] = value;
					});
				}
				updateType(value.type, value.label, value.required);
			});
		} catch(e) {
			if (debug) {
				console.log("buildPreview(): " + e);
			}
		}
	}
	function customOptions (id, thisType) {
		try {
			var thisOptions = '';
			for (i=0; i<optionsCache[id].options.length; i++) {
				if (optionsCache[id].options[i] !== undefined) {
					if (thisType === "radio") {
						thisOptions = thisOptions + '<div id="jcd-fb-radio-' + id + '-' + i + '"><input type="radio" id="jcd-fb-field' + id + '" name="radio-' + id + '" /><span>' + JcdFB.esc_html( optionsCache[id].options[i] ) + '</span><div class="clear"></div></div>';
					} else {
						thisOptions = thisOptions + '<option id="jcd-fb-' + id + '-' + i + '" value="' + id + '-' + i + '">' + JcdFB.esc_html( optionsCache[id].options[i] ) + '</option>';
					}
				}
			}
			return thisOptions;
		} catch(e) {
			if (debug) {
				console.log("customOptions(): " + e);
			}
		}
	}
	function deleteField (that) {
		try {
			grunionNewCount--;
			var thisId = that.attr("id");
			delete fbForm.fields[thisId];
			jQuery("#"+thisId).parent().parent().remove();
			if (grunionNewCount <= maxNewFields) {
				jQuery('#jcd-fb-new-field').show();
			}
		} catch(e) {
			if (debug) {
				console.log("deleteField(): " + e);
			}
		}
	}
	function editField (that) {
		try {
			scroll(0,0);
			setTimeout(function () { jQuery('#jcd-fb-new-label').focus().select(); }, 100);
			var thisId = that.parent().attr('fieldid');
			loadFieldEditor(thisId);
		} catch(e) {
			if (debug) {
				console.log("editField(): " + e);
			}
		}
	}
	function grabShortcode () {
		try {
			// Takes fbForm object and returns shortcode syntax
			jQuery.post(ajaxurl, fbForm, function(response) {
				shortcode = response;
			});
		} catch(e) {
			alert( JcdFB_i18n.problemGeneratingForm );
			if (debug) {
				console.log("grabShortcode(): " + e);
			}
		}
	}
	function hideDesc () {
		jQuery('#jcd-fb-desc').hide();
		jQuery('#jcd-fb-add-field').show();
	}
	function hidePopup () {
		try {
			// copied from wp-includes/js/thickbox/thickbox.js
			jQuery("#TB_imageOff", window.parent.document).unbind("click");
			jQuery("#TB_closeWindowButton", window.parent.document).unbind("click");
			jQuery("#TB_window", window.parent.document).fadeOut("fast");
			jQuery('#TB_window,#TB_overlay,#TB_HideSelect', window.parent.document).trigger("unload").unbind().remove();
			jQuery("#TB_load", window.parent.document).remove();
			if (typeof window.parent.document.body.style.maxHeight == "undefined") {//if IE 6
				jQuery("body","html", window.parent.document).css({height: "auto", width: "auto"});
				jQuery("html", window.parent.document).css("overflow","");
			}
			window.parent.document.onkeydown = "";
			window.parent.document.onkeyup = "";
			return false;
		} catch(e) {
			if (debug) {
				console.log("hidePopup(): " + e);
			}
		}
	}
	function hideShowEditLink (whichType, that) {
		try {
			if (whichType === "show") {
				// Prevents showing links twice
				if (jQuery(".jcd-fb-edit-field").is(":visible")) {
					jQuery(".jcd-fb-edit-field").remove();
				}
				that.find('label').prepend('<span class="right jcd-fb-edit-field" style="font-weight: normal;"><a href="" class="jcd-fb-reorder"><div style="display: none;">' + JcdFB_i18n.moveInstructions + '</div>' + JcdFB_i18n.moveLabel + '</a>&nbsp;&nbsp;<span style="color: #C7D8DE;">|</span>&nbsp;&nbsp;<a href="" class="jcd-fb-edit">' + JcdFB_i18n.editLabel + '</a></span>');
			} else {
				jQuery('.jcd-fb-edit-field').remove();
			}
		} catch(e) {
			if (debug) {
				console.log("hideShowEditLink(): " + e);
			}
		}
	}
	function loadFieldEditor (id) {
		try {
			var thisType = fbForm.fields[id].type;
			jQuery('#jcd-fb-options').hide();
			// Reset hidden field ID
			jQuery('#jcd-fb-field-id').val(id);
			// Load label
			jQuery('#jcd-fb-new-label').val(fbForm.fields[id].label);
			// Load type
			jQuery('#jcd-fb-new-type').val(fbForm.fields[id].type);
			jQuery('.select_wrapper span').text( jQuery('#jcd-fb-new-type option:selected').text() );
			// Load required
			if (fbForm.fields[id].required) {
				jQuery('#jcd-fb-new-required').prop("checked", true);
			} else {
				jQuery('#jcd-fb-new-required').prop("checked", false);
			}
			// Load options if there are any
			if (thisType === "select" || thisType === "radio") {
				var thisResult = '';
				var thisOptions = fbForm.fields[id].options;
				jQuery('#jcd-fb-options').show();
				jQuery('#jcd-fb-new-options').html(""); // Clear it all out
				for (i=0; i<thisOptions.length; i++) {
					if (thisOptions[i] !== undefined) {
						if (thisType === "radio") {
							jQuery('#jcd-fb-new-options').append('<div id="jcd-fb-option-box-' + i + '" class="jcd-fb-new-fields"><span optionid="' + i + '" class="jcd-fb-remove-option"></span><label></label><input type="text" id="jcd-fb-option' + i + '" optionid="' + i + '" value="' + JcdFB.esc_attr( fbForm.fields[id].options[i] ) + '" class="jcd-fb-options" /><div>');
						} else {
							jQuery('#jcd-fb-new-options').append('<div id="jcd-fb-option-box-' + i + '" class="jcd-fb-new-fields"><span optionid="' + i + '" class="jcd-fb-remove-option"></span><label></label><input type="text" id="jcd-fb-option' + i + '" optionid="' + i + '" value="' + JcdFB.esc_attr( fbForm.fields[id].options[i] ) + '" class="jcd-fb-options" /><div>');
						}
					}
				}
			}
			// Load editor & hide description
			hideDesc();
		} catch(e) {
			if (debug) {
				console.log("loadFieldEditor(): " + e);
			}
		}
	}
	function parseShortcode (data) {
		try {
			// Clean up fields by resetting them
			fbForm.fields = {};
			// Add new fields
			if (!data) {
				fbForm.fields = defaultFields;
			} else {
				jQuery.each(data.fields, function(index, value) {
					if ( 1 == value.required )
						value.required = 'true';
					fbForm.fields[index] = value;
				});
				fbForm.to = data.to;
				fbForm.subject = data.subject;
			}
		} catch(e) {
			if (debug) {
				console.log("parseShortcode(): " + e);
			}
		}
	}
	function removeOption (optionId) {
		try {
			var thisId = jQuery('#jcd-fb-field-id').val();
			var thisVal = jQuery('#jcd-fb-option' + optionId).val();
			var thisType = jQuery('#jcd-fb-new-type').val();
			// Remove from right
			jQuery('#jcd-fb-option-box-' + optionId).remove();
			// Remove from preview
			if (thisType === "radio") {
				jQuery('#jcd-fb-radio-' + thisId + '-' + optionId).remove();
			} else {
				jQuery('#jcd-fb-' + thisId + '-' + optionId).remove();
			}
			// Remove from fbForm object
			var idx = fbForm.fields[thisId].options.indexOf(thisVal);
			if (idx !== -1) { fbForm.fields[thisId].options.splice(idx, 1); }
		} catch(e) {
			if (debug) {
				console.log("removeOption(): " + e);
			}
		}
	}
	function removeOptions () {
		try {
			var thisId = jQuery('#jcd-fb-field-id').val();
			jQuery('#jcd-fb-options').hide();
			if (optionsCache[thisId] === undefined) { optionsCache[thisId] = {}; }
			optionsCache[thisId].options = fbForm.fields[thisId].options; // Save options in case they change their mind
			fbForm.fields[thisId].options = []; // Removes all options
		} catch(e) {
			if (debug) {
				console.log("removeOptions(): " + e);
			}
		}
	}
	function sendShortcodeToEditor () {
		try {
			// Serialize fields
			jQuery('div#sortable div.jcd-fb-new-fields').each(function(index) {
				var thisId = jQuery(this).attr('fieldid');
				fbForm.fields[thisId].order = index;
			});
			// Export to WYSIWYG editor
			jQuery.post(ajaxurl, fbForm, function(response) {
				var isVisual = jQuery('#edButtonPreview', window.parent.document).hasClass('active');
				/* WP 3.3+ */
				if ( !isVisual ) {
					isVisual = jQuery( '#wp-content-wrap', window.parent.document ).hasClass( 'tmce-active' );
				}

				var win = window.dialogArguments || opener || parent || top;
				if (isVisual) {
					var currentCode = win.tinyMCE.activeEditor.getContent();
				} else {
					var currentCode = jQuery('#editorcontainer textarea', window.parent.document).val();
					/* WP 3.3+ */
					if ( typeof currentCode != 'string' ) {
						currentCode = jQuery( '.wp-editor-area', window.parent.document ).val();
					}
				}
				var regexp = new RegExp("\\[jcd-contact-form\\b.*?\\/?\\](?:[\\s\\S]+?\\[\\/jcd-contact-form\\])?");

				// Remove new lines that cause BR tags to show up
				response = response.replace(/\n/g,' ');

				// Add new shortcode
				if (currentCode.match(regexp)) {
					if (isVisual) {
						win.tinyMCE.activeEditor.execCommand('mceSetContent', false, currentCode.replace(regexp, response));
					} else {
						// looks like the visual editor is disabled,
						// update the contents of the post directly
						jQuery( '#content', window.parent.document ).val( currentCode.replace( regexp, response ) );
					}
				} else {
					try {
						win.send_to_editor( response );
					} catch ( e ) {
						if (isVisual) {
							win.tinyMCE.activeEditor.execCommand('mceInsertContent', false, response);
						} else {
							// looks like the visual editor is disabled,
							// update the contents of the post directly
							jQuery( '#content', window.parent.document ).val( currentCode + response );
						}
					}
				}
				hidePopup();
			});
		} catch(e) {
			if (debug) {
				console.log("sendShortcodeToEditor(): " + e);
			}
		}
	}
	function showDesc () {
		jQuery('#jcd-fb-desc').show();
		jQuery('#jcd-fb-add-field').hide();
	}
	function showAndHideMessage (message) {
		try {
			var newMessage = (!message) ? JcdFB_i18n.savedMessage : message;
			jQuery('#jcd-fb-success').html(newMessage);
			jQuery('#jcd-fb-success').slideDown('fast');
			setTimeout(function () {
				 jQuery('#jcd-fb-success').slideUp('fast');
			}, 2500);
		} catch(e) {
			if (debug) {
				console.log("showAndHideMessage(): " + e);
			}
		}
	}
	function switchTabs (whichType) {
		try {
			if (whichType === "preview") {
				jQuery('#tab-preview a').addClass('current');
				jQuery('#tab-settings a').removeClass('current');
				jQuery('#jcd-fb-preview-form, #jcd-fb-desc').show();
				jQuery('#jcd-fb-email-settings, #jcd-fb-email-desc').hide();
			} else {
				jQuery('#tab-preview a').removeClass('current');
				jQuery('#tab-settings a').addClass('current');
				jQuery('#jcd-fb-preview-form, #jcd-fb-desc, #jcd-fb-add-field').hide();
				jQuery('#jcd-fb-email-settings, #jcd-fb-email-desc').show();
				jQuery('#jcd-fb-field-my-email').focus().select();
			}
		} catch(e) {
			if (debug) {
				console.log("switchTabs(): " + e);
			}
		}
	}
	function updateLabel () {
		try {
			var thisId = jQuery('#jcd-fb-field-id').val();
			var thisLabel = jQuery('#jcd-fb-new-label').val();
			// Update preview
			if (thisLabel.length === 0) {
				jQuery('#jcd-fb-new-field' + thisId + ' label .label-text').html("New field");
			} else {
				jQuery('#jcd-fb-new-field' + thisId + ' label .label-text').html(thisLabel);
			}
			// Update fbForm object
			fbForm.fields[thisId].label = thisLabel;
		} catch(e) {
			if (debug) {
				console.log("updateLabel(): " + e);
			}
		}
	}
	function updateMyEmail () {
		try {
			var thisEmail = jQuery('#jcd-fb-field-my-email').val();
			fbForm.to = thisEmail;
		} catch(e) {
			if (debug) {
				console.log("updateMyEmail(): " + e);
			}
		}
	}
	function updateOption (that) {
		try {
			var thisId = jQuery('#jcd-fb-field-id').val();
			var thisOptionid = that.attr('optionid');
			var thisOptionValue = that.val();
			var thisType = jQuery('#jcd-fb-new-type').val();
			// Update preview
			if (thisType === "radio") {
				jQuery('#jcd-fb-radio-' + thisId + '-' + thisOptionid + ' span').html(thisOptionValue);
			} else {
				jQuery('#jcd-fb-' + thisId + '-' + thisOptionid).text(thisOptionValue);
			}
			// Update fbForm object
			fbForm.fields[thisId].options[thisOptionid] = thisOptionValue;
		} catch(e) {
			if (debug) {
				console.log("updateOption(): " + e);
			}
		}
	}
	function updateRequired () {
		try {
			var thisId = jQuery('#jcd-fb-field-id').val();
			var thisChecked = jQuery('#jcd-fb-new-required').is(':checked');
			// Update object and preview
			if (thisChecked) {
				fbForm.fields[thisId].required = true;
				jQuery('#jcd-fb-new-field' + thisId + ' label').append('<span class="label-required">' + JcdFB_i18n.requiredLabel + '</span>');
			} else {
				fbForm.fields[thisId].required = false;
				jQuery('#jcd-fb-new-field' + thisId + ' label .label-required').remove();
			}
		} catch(e) {
			if (debug) {
				console.log("updateRequired(): " + e);
			}
		}
	}
	function updateSubject () {
		try {
			var thisSubject = jQuery('#jcd-fb-field-subject').val();
			fbForm.subject = thisSubject;
		} catch(e) {
			if (debug) {
				console.log("updateSubject(): " + e);
			}
		}
	}
	function updateType(thisType, thisLabelText, thisRequired) {
		try {
			var isLoaded = thisType;
			var thisId = jQuery('#jcd-fb-field-id').val();
			if (!thisType) { var thisType = jQuery('#jcd-fb-new-type').val(); }
			if (!thisLabelText) { var thisLabelText = jQuery('#jcd-fb-new-field' + thisId + ' .label-text').html(); }
			var isRequired = (thisRequired) ? '<span class="label-required">' + JcdFB_i18n.requiredLabel + '</span>' : '';
			var thisLabel = '<label fieldid="' + thisId + '" for="jcd-fb-field' +  thisId + '"><span class="label-text">' + JcdFB.esc_html( thisLabelText ) + '</span>' + isRequired + '</label>';
			var thisRadio = '<input type="radio" name="radio-' + thisId + '" id="jcd-fb-field' + thisId + ' "disabled="disabled" />';
			var thisRadioLabel = '<label fieldid="' + thisId + '" for="jcd-fb-field' +  thisId + '" class="jcd-fb-radio-label"><span class="label-text">' + JcdFB.esc_html( thisLabelText ) + '</span>' + isRequired + '</label>';
			var thisRadioRemove = '<div class="jcd-fb-remove fb-remove-small" id="' +  thisId + '"><span></span></div>';
			var thisRemove = '<div class="jcd-fb-remove" id="' +  thisId + '"><span></span></div>';
			var thisCheckbox = '<input type="checkbox" id="jcd-fb-field' + thisId + '" disabled="disabled" />';
			var thisText = '<input type="text" id="jcd-fb-field' + thisId + '" disabled="disabled" />';
			var thisTextarea = '<textarea id="jcd-fb-field' + thisId + '" disabled="disabled"></textarea>';
			var thisClear = '<div class="clear"></div>';
			var thisSelect = '<select id="jcd-fb-field' + thisId + '" fieldid="' + thisId + '"><option id="jcd-fb-' + thisId + '-' + optionsCount + '" value="' + thisId + '-' + optionsCount + '">' + JcdFB_i18n.firstOptionLabel + '</option></select>';
			switch (thisType) {
				case "checkbox":
					removeOptions();
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisRadioRemove + thisCheckbox + thisRadioLabel + thisClear);
					break;
				case "email":
					removeOptions();
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisRemove + thisLabel + thisText);
					break;
				case "name":
					removeOptions();
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisRemove + thisLabel + thisText);
					break;
				case "radio":
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisLabel + thisRadioRemove + '<div fieldid="' + thisId + '" id="jcd-fb-custom-radio' + thisId + '"></div>');
					if (optionsCache[thisId] !== undefined && optionsCache[thisId].options.length !== 0) {
						fbForm.fields[thisId].options = optionsCache[thisId].options;
						jQuery('#jcd-fb-custom-radio' + thisId).append(customOptions(thisId, thisType));
					} else {
						jQuery('#jcd-fb-new-options').html('<label for="jcd-fb-option0">' + JcdFB_i18n.optionsLabel + '</label><input type="text" id="jcd-fb-option0" optionid="0" value="' + JcdFB_i18n.firstOptionLabel + '" class="jcd-fb-options" />');
						jQuery('#jcd-fb-custom-radio' + thisId).append('<div id="jcd-fb-radio-' + thisId + '-0">' + thisRadio + '<span>' + JcdFB_i18n.firstOptionLabel + '</span>' + thisClear + '</div>');
						fbForm.fields[thisId].options[optionsCount] = JcdFB_i18n.firstOptionLabel;
					}
					jQuery('#jcd-fb-options').show();
					setTimeout(function () { jQuery('#jcd-fb-option0').focus().select(); }, 100);
					break;
				case "select":
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisRemove + thisLabel + thisSelect);
					if (optionsCache[thisId] !== undefined && optionsCache[thisId].options.length !== 0) {
						fbForm.fields[thisId].options = optionsCache[thisId].options;
						jQuery('#jcd-fb-field' + thisId).html(customOptions(thisId, thisType));
					} else {
						jQuery('#jcd-fb-new-options').html('<label for="jcd-fb-option0">' + JcdFB_i18n.optionsLabel + '</label><input type="text" id="jcd-fb-option0" optionid="0" value="' + JcdFB_i18n.firstOptionLabel + '" class="jcd-fb-options" />');
						fbForm.fields[thisId].options[optionsCount] = JcdFB_i18n.firstOptionLabel;
					}
					jQuery('#jcd-fb-options').show();
					setTimeout(function () { jQuery('#jcd-fb-option0').focus().select(); }, 100);
					break;
				case "text":
					removeOptions();
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisRemove + thisLabel + thisText);
					break;
				case "textarea":
					removeOptions();
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisRemove + thisLabel + thisTextarea);
					break;
				case "url":
					removeOptions();
					jQuery('#jcd-fb-new-field' + thisId + ' .jcd-fb-fields').html(thisRemove + thisLabel + thisText);
					break;
			}
			// update object
			fbForm.fields[thisId].type = thisType;
		} catch(e) {
			if (debug) {
				console.log("updateType(): " + e);
			}
		}
	}


  function styleSelect() {
    jQuery( '.select_wrapper').each(function () {
    	console.log(jQuery(this).find( '.jcd-input option:selected').text());
      jQuery(this).prepend( '<span>' + jQuery(this).find( '.jcd-input option:selected').text() + '</span>' );
    });
    jQuery( 'select.jcd-input').live( 'change', function () {
      jQuery(this).prev( 'span').replaceWith( '<span>' + jQuery(this).find( 'option:selected').text() + '</span>' );
    });
    jQuery( 'select.jcd-input').bind(jQuery.browser.msie ? 'click' : 'change', function(event) {
      jQuery(this).prev( 'span').replaceWith( '<span>' + jQuery(this).find( 'option:selected').text() + '</span>' );
    }); 
  }

	return {
		resizePop: function () {
			try {
				//Thickbox won't resize for some reason, we are manually doing it here
				var totalWidth = jQuery('body', window.parent.document).width();
				var totalHeight = jQuery('body', window.parent.document).height();
				var isIE6 = typeof document.body.style.maxHeight === "undefined";

				jQuery('#TB_window, #TB_iframeContent', window.parent.document).css('width', '768px');
				jQuery('#TB_window', window.parent.document).css({ left: (totalWidth-768)/2 + 'px', top: '23px', position: 'absolute', marginLeft: '0' });
				if ( ! isIE6 ) { // take away IE6
					jQuery('#TB_window, #TB_iframeContent', window.parent.document).css('height', (totalHeight-73) + 'px');
				}
			} catch(e) {
				if (debug) {
					console.log("resizePop(): " + e);
				}
			}
		},
		init: function () {
			// Scroll to top of page
			window.parent.scroll(0,0);
			//Check for existing form data
			if (jQuery('#edButtonPreview', window.parent.document).hasClass('active') || jQuery( '#wp-content-wrap', window.parent.document ).hasClass( 'tmce-active' ) ) {
				var win = window.dialogArguments || opener || parent || top;
				var contentSource = win.tinyMCE.activeEditor.getContent();
			} else {
				var contentSource = jQuery('#content', window.parent.document).val();
			}
			var data = {
				action: 'jcd_formbuilder_shortcode_to_json',
				'_ajax_nonce' : ajax_nonce_json,
				post_id: postId,
				content: contentSource
			};

			styleSelect();

			jQuery.post(ajaxurl, data, function(response) {
				// Setup fbForm
				parseShortcode(jQuery.parseJSON(response));
				// Now build out the preview form
				buildPreview();
			});
			// actions
			jQuery('.jcd-fb-add-field').click(function () {
				addField();
				hideDesc();
				return false;
			});
			jQuery('#jcd-fb-new-label').keyup(function () {
				updateLabel();
			});
			jQuery('#jcd-fb-new-type').change(function () {
				updateType();
			});
			jQuery('#jcd-fb-new-required').click(function () {
				updateRequired();
			});
			jQuery('.jcd-fb-remove').live('click', function () {
				showDesc();
				deleteField(jQuery(this));
				grabShortcode();
			});
			jQuery('#jcd-fb-preview').submit(function () {
				sendShortcodeToEditor();
				return false;
			});
			jQuery('#TB_overlay, #TB_closeWindowButton', window.parent.document).mousedown(function () {
				if(confirm( JcdFB_i18n.exitConfirmMessage )) {
					hidePopup();
				}
			});
			jQuery('#jcd-fb-another-option').live('click', function () {
				addOption();
			});
			jQuery('.jcd-fb-options').live('keyup', function () {
				updateOption(jQuery(this));
			});
			jQuery('.jcd-fb-remove-option').live('click', function () {
				removeOption(jQuery(this).attr('optionid'));
			});
			jQuery('#tab-preview a').click(function () {
				switchTabs('preview');
				return false;
			});
			jQuery('#jcd-fb-prev-form').click(function () {
				switchTabs('preview');
				showAndHideMessage( JcdFB_i18n.savedMessage );
				return false;
			});
			jQuery('#tab-settings a').click(function () {
				switchTabs();
				return false;
			});
			jQuery('#jcd-fb-field-my-email').blur(function () {
				updateMyEmail();
			});
			jQuery('#jcd-fb-field-subject').blur(function () {
				updateSubject();
			});
			jQuery('.jcd-fb-form-case .jcd-fb-new-fields').live('mouseenter', function () {
				hideShowEditLink('show', jQuery(this));
			});
			jQuery('.jcd-fb-form-case .jcd-fb-new-fields').live('mouseleave', function () {
				hideShowEditLink('hide');
				return false;
			});
			jQuery('.jcd-fb-edit-field').live('click', function () {
				editField(jQuery(this));
				return false;
			});
			jQuery('.jcd-fb-edit-field .jcd-fb-reorder').live('click', function () {
				return false;
			});
			jQuery('#jcd-fb-save-field').live('click', function () {
				showDesc();
				showAndHideMessage();
				return false;
			});
			jQuery('#jcd-fb-feedback').click(function () {
				var thisHref = jQuery(this).attr('href');
				window.parent.location = thisHref;
				return false;
			});
			jQuery("#sortable").sortable({
				axis: 'y',
				handle: '.jcd-fb-reorder',
				items: '.jcd-fb-new-fields',
				connectWith: '#jcd-fb-extra-fields',
				start: function() { jQuery('.jcd-fb-edit-field').hide(); }
			});
			jQuery("#jcd-fb-extra-fields").sortable({
				axis: 'y',
				handle: '.jcd-fb-reorder',
				items: '.jcd-fb-new-fields',
				connectWith: '#sortable',
				start: function() { jQuery('.jcd-fb-edit-field').hide(); }
			});
			jQuery("#draggable").draggable({
				axis: 'y',
				handle: '.jcd-fb-reorder',
				connectToSortable: '#sortable',
				helper: 'clone',
				revert: 'invalid'
			});
		}
	};
}();
