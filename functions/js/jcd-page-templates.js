jQuery(document).ready(function() {	
	var $ptemplate_select = jQuery('select#page_template'),
		$ptemplate_box = jQuery('#normal-sortables');
                
        var rem, sidebars = jQuery('div.widgets-sortables'), isRTL = !! ( 'undefined' != typeof isRtl && isRtl ),
                margin = ( isRtl ? 'marginRight' : 'marginLeft' ), the_id;
		
	$ptemplate_select.live('change',function(){
		var this_value = jQuery(this).val();
		$ptemplate_box.find('#jcd_ptemplate_meta').css('display','none');
		jQuery('#postdivrich').show();
		
		switch ( this_value ) {
			case 'template-homepage.php':
				$ptemplate_box.find('#jcd_ptemplate_meta').css('display','block');
                                jQuery('#postdivrich').hide();
				break;
			default:
                                $ptemplate_box.find('#jcd_ptemplate_meta').css('display','hidden');
                                jQuery('#postdivrich').show();
                                break;
		}
	});
	
	$ptemplate_select.trigger('change');
        
});
