/**
 * Jcd Admin Interface JavaScript
 *
 * All JavaScript logic for the theme options admin interface.
 * @since 4.8.0
 *
 */

(function ($) {

  jcdAdminInterface = {
  
/**
 * toggle_nav_tabs()
 *
 * @since 4.8.0
 */
 
 	toggle_nav_tabs: function () {
 		var flip = 0;
	
		$( '#expand_options' ).click( function(){
			if( flip == 0 ){
				flip = 1;
				$( '#jcd_container #jcd-nav' ).hide();
				//$( '#jcd_container #content' ).width( 785 );
				$( '#jcd_container .group' ).add( '#jcd_container .group h1' ).show();

				$(this).text( '[-]' );

			} else {
				flip = 0;
				$( '#jcd_container #jcd-nav' ).show();
				$( '#jcd_container #content' ).width( 595 );
				$( '#jcd_container .group' ).add( '#jcd_container .group h1' ).hide();
				$( '#jcd_container .group:first' ).show();
				$( '#jcd_container #jcd-nav li' ).removeClass( 'current' );
				$( '#jcd_container #jcd-nav li:first' ).addClass( 'current' );

				$(this).text( '[+]' );

			}

		});
 	}, // End toggle_nav_tabs()

/**
 * load_first_tab()
 *
 * @since 4.8.0
 */
 
 	load_first_tab: function () {
 		$( '.group' ).hide();
 		$( '.group:has(".section"):first' ).fadeIn(); // Fade in the first tab containing options (not just the first tab).
 	}, // End load_first_tab()
 	
/**
 * open_first_menu()
 *
 * @since 5.0.0
 */
 
 	open_first_menu: function () {
 		$( '#jcd-nav li.current.has-children:first ul.sub-menu' ).slideDown().addClass( 'open' ).children( 'li:first' ).addClass( 'active' ).parents( 'li.has-children' ).addClass( 'open' );
 	}, // End open_first_menu()
 	
/**
 * toggle_nav_menus()
 *
 * @since 5.0.0
 */
 
 	toggle_nav_menus: function () {
 		$( '#jcd-nav li.has-children > a' ).click( function ( e ) {
 			if ( $( this ).parent().hasClass( 'open' ) ) { return false; }
 			
 			$( '#jcd-nav li.top-level' ).removeClass( 'open' ).removeClass( 'current' );
 			$( '#jcd-nav li.active' ).removeClass( 'active' );
 			if ( $( this ).parents( '.top-level' ).hasClass( 'open' ) ) {} else {
 				$( '#jcd-nav .sub-menu.open' ).removeClass( 'open' ).slideUp().parent().removeClass( 'current' );
 				$( this ).parent().addClass( 'open' ).addClass( 'current' ).find( '.sub-menu' ).slideDown().addClass( 'open' ).children( 'li:first' ).addClass( 'active' );
 			}
 			
 			// Find the first child with sections and display it.
 			var clickedGroup = $( this ).parent().find( '.sub-menu li a:first' ).attr( 'href' );
 			
 			if ( clickedGroup != '' ) {
 				$( '.group' ).hide();
 				$( clickedGroup ).fadeIn();
 			}
 			return false;
 		});
 	}, // End toggle_nav_menus()
 	
/**
 * toggle_collapsed_fields()
 *
 * @since 4.8.0
 */
 
 	toggle_collapsed_fields: function () {
		$( '.group .collapsed' ).each(function(){
			$( this ).find( 'input:checked' ).parent().parent().parent().nextAll().each( function(){
				if ($( this ).hasClass( 'last' ) ) {
					$( this ).removeClass( 'hidden' );
					return false;
				}
				$( this ).filter( '.hidden' ).removeClass( 'hidden' );
				
				$( '.group .collapsed input:checkbox').click(function ( e ) {
					jcdAdminInterface.unhide_hidden( $( this ).attr( 'id' ) );
				});

			});
		});
 	}, // End toggle_collapsed_fields()

/**
 * setup_nav_highlights()
 *
 * @since 4.8.0
 */
 
 	setup_nav_highlights: function () {
	 	// Highlight the first item by default.
	 	$( '#jcd-nav li.top-level:first' ).addClass( 'current' ).addClass( 'open' );
		
		// Default single-level logic.
		$( '#jcd-nav li.top-level' ).not( '.has-children' ).find( 'a' ).click( function ( e ) {
			var thisObj = $( this );
			var clickedGroup = thisObj.attr( 'href' );
			
			if ( clickedGroup != '' ) {
				$( '#jcd-nav .open' ).removeClass( 'open' );
				$( '.sub-menu' ).slideUp();
				$( '#jcd-nav .active' ).removeClass( 'active' );
				$( '#jcd-nav li.current' ).removeClass( 'current' );
				thisObj.parent().addClass( 'current' );
				
				$( '.group' ).hide();
				$( clickedGroup ).fadeIn();
				
				return false;
			}
		});
		
		$( '#jcd-nav li:not(".has-children") > a:first' ).click( function( evt ) {
			var parentObj = $( this ).parent( 'li' );
			var thisObj = $( this );
			
			var clickedGroup = thisObj.attr( 'href' );
			
			if ( $( this ).parents( '.top-level' ).hasClass( 'open' ) ) {} else {
				$( '#jcd-nav li.top-level' ).removeClass( 'current' ).removeClass( 'open' );
				$( '#jcd-nav .sub-menu' ).removeClass( 'open' ).slideUp();
				$( this ).parents( 'li.top-level' ).addClass( 'current' );
			}
		
			$( '.group' ).hide();
			$( clickedGroup ).fadeIn();
		
			evt.preventDefault();
			return false;
		});
		
		// Sub-menu link click logic.
		$( '.sub-menu a' ).click( function ( e ) {
			var thisObj = $( this );
			var parentMenu = $( this ).parents( 'li.top-level' );
			var clickedGroup = thisObj.attr( 'href' );
			
			if ( $( '.sub-menu li a[href="' + clickedGroup + '"]' ).hasClass( 'active' ) ) {
				return false;
			}
			
			if ( clickedGroup != '' ) {
				parentMenu.addClass( 'open' );
				$( '.sub-menu li, .flyout-menu li' ).removeClass( 'active' );
				$( this ).parent().addClass( 'active' );
				$( '.group' ).hide();
				$( clickedGroup ).fadeIn();
			}
			
			return false;
		});
 	}, // End setup_nav_highlights()

/**
 * Load tabs based on hash on URL
 */

	load_tabs_by_hash: function() {
		var hash = window.location.hash;

		if( hash != '' ) {
			if( $(hash).length > 0 ) {
				$( '#jcd-nav li.current' ).removeClass( 'current' );
				$('[href="'+ hash +'"]').parent('li').addClass('current');
				$( '.group' ).hide();
				$( hash ).fadeIn();
			}
		}
	},

/**
 * setup_custom_typography()
 *
 * @since 4.8.0
 */
 
 	setup_custom_typography: function () {
	 	$( 'select.jcd-typography-unit' ).change( function(){
			var val = $( this ).val();
			var parent = $( this ).parent();
			var name = parent.find( '.jcd-typography-size-px' ).attr( 'name' );
			if( name == '' ) { var name = parent.find( '.jcd-typography-size-em' ).attr( 'name' ); }
		
			if( val == 'px' ) {
				parent.find( '.jcd-typography-size-em' ).hide().removeAttr( 'name' );
				parent.find( '.jcd-typography-size-px' ).show().attr( 'name', name );
			}
			else if( val == 'em' ) {
				parent.find( '.jcd-typography-size-em' ).show().attr( 'name', name );
				parent.find( '.jcd-typography-size-px' ).hide().removeAttr( 'name' );
			}
		
		});
 	}, // End setup_custom_typography()

/**
 * init_flyout_menus()
 *
 * @since 5.0.0
 */
 
 	init_flyout_menus: function () {
 		// Only trigger flyouts on menus with closed sub-menus.
 		$( '#jcd-nav li.has-children' ).each ( function ( i ) {
 			$( this ).hover(
	 			function () {
	 				if ( $( this ).find( '.flyout-menu' ).length == 0 ) {
		 				var flyoutContents = $( this ).find( '.sub-menu' ).html();
		 				var flyoutMenu = $( '<div />' ).addClass( 'flyout-menu' ).html( '<ul>' + flyoutContents + '</ul>' );
		 				$( this ).append( flyoutMenu );
	 				}
	 			}, 
	 			function () {
	 				// $( '#jcd-nav .flyout-menu' ).remove();
	 			}
	 		);
 		});
 		
 		// Add custom link click logic to the flyout menus, due to custom logic being required.
 		$( '.flyout-menu a' ).live( 'click', function ( e ) {
 			var thisObj = $( this );
 			var parentObj = $( this ).parent();
 			var parentMenu = $( this ).parents( '.top-level' );
 			var clickedGroup = $( this ).attr( 'href' );
 			
 			if ( clickedGroup != '' ) {
	 			$( '.group' ).hide();
	 			$( clickedGroup ).fadeIn();
	 			
	 			// Adjust the main navigation menu.
	 			$( '#jcd-nav li' ).removeClass( 'open' ).removeClass( 'current' ).find( '.sub-menu' ).slideUp().removeClass( 'open' );
	 			parentMenu.addClass( 'open' ).addClass( 'current' ).find( '.sub-menu' ).slideDown().addClass( 'open' );
	 			$( '#jcd-nav li.active' ).removeClass( 'active' );
	 			$( '#jcd-nav a[href="' + clickedGroup + '"]' ).parent().addClass( 'active' );
 			}
 			
 			return false;
 		});
 	}, // End init_flyout_menus()

/**
 * unhide_hidden()
 *
 * @since 4.8.0
 * @see toggle_collapsed_fields()
 */
 
 	unhide_hidden: function ( obj ) {
 		obj = $( '#' + obj ); // Get the jQuery object.
 		
		if ( obj.attr( 'checked' ) ) {
			obj.parent().parent().parent().nextAll().slideDown().removeClass( 'hidden' ).addClass( 'visible' );
		} else {
			obj.parent().parent().parent().nextAll().each( function(){
				if ( $( this ).filter( '.last' ).length ) {
					$( this ).slideUp().addClass( 'hidden' );
				return false;
				}
				$( this ).slideUp().addClass( 'hidden' );
			});
		}
 	}, // End unhide_hidden()

/**
 * image_checkbox_switch()
 * 
 */

 	image_checkbox_switch: function() {
 		$('.jcd-radio-img-img').parent('span').click(function(e){
 			$(this)
 				.addClass('jcd-radio-img-selected')
 				.siblings().removeClass('jcd-radio-img-selected');
 		});
 	}, // End image_checkbox_swtich()


/**
 * set_colorpicker()
 */

 	set_colorpicker: function() {
 		$('.colorSelector').each(function(){
 			var $colorpicker = $(this);

 			$colorpicker.ColorPicker({
 				onChange: function(hsb, hex, rgb) {
 					$colorpicker.children('div').css( 'backgroundColor', '#' + hex);
 					$colorpicker.next('input').attr('value', '#' + hex);
 				}
 			});
 		});
 	} // End set_colorpicker()

  }; // End jcdAdminInterface Object // Don't remove this, or the sky will fall on your head.

/**
 * Execute the above methods in the jcdAdminInterface object.
 *
 * @since 4.8.0
 */
	$(document).ready(function () {
	
		jcdAdminInterface.toggle_nav_tabs();
		jcdAdminInterface.load_first_tab();
		jcdAdminInterface.toggle_collapsed_fields();
		jcdAdminInterface.setup_nav_highlights();
		jcdAdminInterface.toggle_nav_menus();
		jcdAdminInterface.init_flyout_menus();
		jcdAdminInterface.open_first_menu();
		jcdAdminInterface.setup_custom_typography();
		jcdAdminInterface.image_checkbox_switch();
		jcdAdminInterface.set_colorpicker();
		jcdAdminInterface.load_tabs_by_hash();
	
	});
  
})(jQuery);