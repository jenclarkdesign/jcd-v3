<?php
/**
 * This file holds various functions to create a coming soon mode
 *
 * @author	Arif Widi @mambows
 * @copyright	Copyright ( c ) Arif Widi
 * @link	http://jcd.me
 * @since	Version 1.2
 * @package 	JcdFramework
 */

/* 
 * Hook into Jcd options array
 * 
 */
function jcd_comingsoon_options( $options ) {

	$options[] = array( "name" => "Coming Soon Page",
	          "type" => "heading",
	          "icon" => "layout" );

	$options[] = array( "name" => "Enable Coming Soon Page",
						"desc" => "Check this if you want to enable coming soon page.",
						"id" => "jcd_comingsoon_enable",
						"type" => "checkbox" );

	$options[] = array( "name" => "Launch Date",
	          "desc" => "Enter date when the website will published. Please input the value, or coming soon page template won't work.",
	          "id" => "jcd_comingsoon_date",
	          "type" => "calendar" );

	$options[] = array( "name" => "Additional Text",
						"desc" => "Additional text that will be shown.",
						"id" => "jcd_comingsoon_text",
						"type" => "textarea" );

	$options[] = array( "name" => "Header Color",
						"desc" => "Check this if your logo have white text, the header background color will be inverted",
						"id" => "jcd_comingsoon_headerbg",
						"type" => "checkbox" );

	$options[] = array( "name" => 'Subscribe By E-mail to MailChimp',
            "desc" => 'If you have a MailChimp account you can enter the MailChimp List Subscribe URL to allow your users to subscribe to a MailChimp List.',
            "id" => "jcd_comingsoon_mailchimp_list_url",
            "type" => "text" );

	$options[] = array( "name" => "Background Image", 
						"desc" => "Change the background image of the Coming Soon page.",
						"id" => "jcd_comingsoon_background", 
						"type" => "upload" );

	return $options;
}
add_filter( 'jcd_options_data', 'jcd_comingsoon_options' );

/**
 * Show coming soon page for unauthorized users
 */
function jcd_show_comingsoon() {

	$is_enable = get_option( 'jcd_comingsoon_enable' );
	$launch_date = get_option( 'jcd_comingsoon_date' );
	

	// Make sure coming soon mode is enable and launch date is not empty
	if( "true" == $is_enable && !empty( $launch_date ) ) {
		// If launch date
		if( strtotime($launch_date) > time() ) {
			if( !is_user_logged_in()
				&& ! strstr($_SERVER['PHP_SELF'], 'feed/')
				&& ! strstr($_SERVER['PHP_SELF'], 'wp-admin/')
				&& ! strstr($_SERVER['PHP_SELF'], 'wp-login.php')
				&& ! strstr($_SERVER['PHP_SELF'], 'async-upload.php')
				&& ! strstr($_SERVER['PHP_SELF'], 'upgrade.php')
				&& ! strstr($_SERVER['PHP_SELF'], 'trackback/')
				&& ! strstr($_SERVER['PHP_SELF'], '/xmlrpc.php') ) {

				global $is_comingsoon;
				$is_comingsoon = true;

				// It will find for template file inside themes, if not found, 
				// the default template will be loaded
				locate_template( array( 'template-comingsoon.php', 'functions/views/jcd-comingsoon-view.php' ), true, true );
				die;
			}
		}
	}

}
add_action( 'init', 'jcd_show_comingsoon' );

/**
 * Enqueue scripts and styles
 */
function jcd_comingsoon_scripts() {
	global $is_comingsoon;

	if( $is_comingsoon ) {
		wp_enqueue_script( 'jquery-countdown', get_template_directory_uri() . '/functions/js/jquery.countdown.min.js', array('jquery') );
		wp_enqueue_style( 'comingsoon-page', get_template_directory_uri() . '/functions/css/jcd-comingsoon.css' );

		if( get_option( 'jcd_comingsoon_background' ) != '' ) {
			wp_enqueue_script( 'jquery-anystretch', get_template_directory_uri() . '/functions/js/jquery.anystretch.js', array('jquery') );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'jcd_comingsoon_scripts' );

/**
 * Add notification on the admin bar, when coming soon mode is active
 */
function jcd_comingsoon_adminbar( $admin_bar ) {
	if( 'true' == get_option( 'jcd_comingsoon_enable' ) && get_option( 'jcd_comingsoon_date' ) ) {
		$admin_bar->add_menu( array(
			'parent' => 'top-secondary',
			'id' => 'jcd_comingsoon_active',
			'href' => admin_url( 'admin.php?page=jcd#jcd-option-comingsoonpage' ),
			'title' => 'Coming Soon Mode Active'
		) );
	}
}
add_action( 'admin_bar_menu', 'jcd_comingsoon_adminbar', 100 );