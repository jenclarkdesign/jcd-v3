<?php
/**
 * Custom fields for WordPress write panels.
 *
 * Add custom fields to various post types "Add" and "Edit" screens within WordPress.
 * Also processes the custom fields as post meta when the post is saved.
 *
 * @category CustomFields
 * @package WordPress
 * @subpackage JcdFramework
 * @author Jcd
 * @since 1.0.0
 *
 * TABLE OF CONTENTS
 *
 * - jcd_metabox_create()
 * - jcd_metabox_handle()
 * - jcd_metabox_add()
 * - jcd_metabox_fieldtypes()
 * - jcd_uploader_custom_fields()
 * - jcd_custom_enqueue()
 * - jcd_custom_enqueue_css()
 */

/**
 * jcd_metabox_create function.
 *
 * @access public
 * @param object $post
 * @param array $callback
 * @return void
 */
function jcd_metabox_create( $post, $callback ) {
    global $post;

    // Allow child themes/plugins to act here.
    do_action( 'jcd_metabox_create', $post, $callback );

    $template_to_show = $callback['args'];

    $jcd_metaboxes = get_option( 'jcd_custom_template' );

    // Array sanity check.
    if ( ! is_array( $jcd_metaboxes ) ) { return; }

    $output = '';
    $output .= '<table class="jcd_metaboxes_table">'."\n";
    foreach ( $jcd_metaboxes as $k => $jcd_metabox ) {

    	// Setup CSS classes to be added to each table row.
    	$row_css_class = 'jcd-custom-field';
    	if ( ( $k + 1 ) == count( $jcd_metaboxes ) ) { $row_css_class .= ' last'; }

    	$jcd_id = 'jcd_' . $jcd_metabox['name'];
    	$jcd_name = $jcd_metabox['name'];

    	if ( $template_to_show == 'seo' ) {
    		$metabox_post_type_restriction = 'undefined';
    	} elseif ( function_exists( 'jcd_content_builder_menu' ) ) {
    		$metabox_post_type_restriction = $jcd_metabox['cpt'][$post->post_type];
    	} else {
    		$metabox_post_type_restriction = 'undefined';
    	}

    	if ( ( $metabox_post_type_restriction != '' ) && ( $metabox_post_type_restriction == 'true' ) ) {
    		$type_selector = true;
    	} elseif ( $metabox_post_type_restriction == 'undefined' ) {
    		$type_selector = true;
    	} else {
    		$type_selector = false;
    	}

   		$jcd_metaboxvalue = '';

    	if ( $type_selector ) {

    		if( isset( $jcd_metabox['type'] ) && ( in_array( $jcd_metabox['type'], jcd_metabox_fieldtypes() ) ) ) {

        	    	$jcd_metaboxvalue = get_post_meta($post->ID,$jcd_name,true);

				}

				// Make sure slashes are stripped before output.
				foreach ( array( 'label', 'desc', 'std' ) as $k ) {
					if ( isset( $jcd_metabox[$k] ) && ( $jcd_metabox[$k] != '' ) ) {
						$jcd_metabox[$k] = stripslashes( $jcd_metabox[$k] );
					}
				}

        	    if ( $jcd_metaboxvalue == '' && isset( $jcd_metabox['std'] ) ) {

        	        $jcd_metaboxvalue = $jcd_metabox['std'];
        	    }

        	    // Add a dynamic CSS class to each row in the table.
        	    $row_css_class .= ' jcd-field-type-' . strtolower( $jcd_metabox['type'] );

				if( $jcd_metabox['type'] == 'info' ) {

        	        $output .= "\t".'<tr class="' . $row_css_class . '" style="background:#f8f8f8; font-size:11px; line-height:1.5em;">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="'. esc_attr( $jcd_id ) .'">'.$jcd_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td style="font-size:11px;">'.$jcd_metabox['desc'].'</td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }
        	    elseif( $jcd_metabox['type'] == 'text' ) {

        	    	$add_class = ''; $add_counter = '';
        	    	if($template_to_show == 'seo'){$add_class = 'words-count'; $add_counter = '<span class="counter">0 characters, 0 words</span>';}
        	        $output .= "\t".'<tr class="' . $row_css_class . '">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="'.esc_attr( $jcd_id ).'">'.$jcd_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><input class="jcd_input_text '.$add_class.'" type="'.$jcd_metabox['type'].'" value="'.esc_attr( $jcd_metaboxvalue ).'" name="'.$jcd_name.'" id="'.esc_attr( $jcd_id ).'"/>';
        	        $output .= '<span class="jcd_metabox_desc">'.$jcd_metabox['desc'] .' '. $add_counter .'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ( $jcd_metabox['type'] == 'textarea' ) {

        	   		$add_class = ''; $add_counter = '';
        	    	if( $template_to_show == 'seo' ){ $add_class = 'words-count'; $add_counter = '<span class="counter">0 characters, 0 words</span>'; }
        	        $output .= "\t".'<tr class="' . $row_css_class . '">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="'.$jcd_id.'">'.$jcd_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><textarea class="jcd_input_textarea '.$add_class.'" name="'.$jcd_name.'" id="'.esc_attr( $jcd_id ).'">' . esc_textarea(stripslashes($jcd_metaboxvalue)) . '</textarea>';
        	        $output .= '<span class="jcd_metabox_desc">'.$jcd_metabox['desc'] .' '. $add_counter.'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ( $jcd_metabox['type'] == 'calendar' ) {

        	        $output .= "\t".'<tr class="' . $row_css_class . '">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="'.$jcd_metabox.'">'.$jcd_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><input class="jcd_input_calendar" type="text" name="'.$jcd_name.'" id="'.esc_attr( $jcd_id ).'" value="'.esc_attr( $jcd_metaboxvalue ).'">';
        	        $output .= "\t\t" . '<input type="hidden" name="datepicker-image" value="' . get_template_directory_uri() . '/functions/images/calendar.gif" />';
        	        $output .= '<span class="jcd_metabox_desc">'.$jcd_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ( $jcd_metabox['type'] == 'time' ) {

        	        $output .= "\t".'<tr>';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="' . esc_attr( $jcd_id ) . '">' . $jcd_metabox['label'] . '</label></th>'."\n";
        	        $output .= "\t\t".'<td><input class="jcd_input_time" type="' . $jcd_metabox['type'] . '" value="' . esc_attr( $jcd_metaboxvalue ) . '" name="' . $jcd_name . '" id="' . esc_attr( $jcd_id ) . '"/>';
        	        $output .= '<span class="jcd_metabox_desc">' . $jcd_metabox['desc'] . '</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }

        	    elseif ( $jcd_metabox['type'] == 'select' ) {

        	        $output .= "\t".'<tr class="' . $row_css_class . '">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="' . esc_attr( $jcd_id ) . '">' . $jcd_metabox['label'] . '</label></th>'."\n";
        	        $output .= "\t\t".'<td><select class="jcd_input_select" id="' . esc_attr( $jcd_id ) . '" name="' . esc_attr( $jcd_name ) . '">';
        	        $output .= '<option value="">Select to return to default</option>';

        	        $array = $jcd_metabox['options'];

        	        if( $array ) {

        	            foreach ( $array as $id => $option ) {
        	                $selected = '';

        	                if( isset( $jcd_metabox['default'] ) )  {
								if( $jcd_metabox['default'] == $option && empty( $jcd_metaboxvalue ) ) { $selected = 'selected="selected"'; }
								else  { $selected = ''; }
							}

        	                if( $jcd_metaboxvalue == $option ){ $selected = 'selected="selected"'; }
        	                else  { $selected = ''; }

        	                $output .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . $option . '</option>';
        	            }
        	        }

        	        $output .= '</select><span class="jcd_metabox_desc">' . $jcd_metabox['desc'] . '</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";
        	    }
        	    elseif ( $jcd_metabox['type'] == 'select2' ) {

        	        $output .= "\t".'<tr class="' . $row_css_class . '">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="' . esc_attr( $jcd_id ) . '">' . $jcd_metabox['label'] . '</label></th>'."\n";
        	        $output .= "\t\t".'<td><select class="jcd_input_select" id="' . esc_attr( $jcd_id ) . '" name="' . esc_attr( $jcd_name ) . '">';
        	        $output .= '<option value="">Select to return to default</option>';

        	        $array = $jcd_metabox['options'];

        	        if( $array ) {

        	            foreach ( $array as $id => $option ) {
        	                $selected = '';

        	                if( isset( $jcd_metabox['default'] ) )  {
								if( $jcd_metabox['default'] == $id && empty( $jcd_metaboxvalue ) ) { $selected = 'selected="selected"'; }
								else  { $selected = ''; }
							}

        	                if( $jcd_metaboxvalue == $id ) { $selected = 'selected="selected"'; }
        	                else  {$selected = '';}

        	                $output .= '<option value="'. esc_attr( $id ) .'" '. $selected .'>' . $option . '</option>';
        	            }
        	        }

        	        $output .= '</select><span class="jcd_metabox_desc">'.$jcd_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";
        	    }

        	    elseif ( $jcd_metabox['type'] == 'checkbox' ){

        	        if( $jcd_metaboxvalue == 'true' ) { $checked = ' checked="checked"'; } else { $checked=''; }

        	        $output .= "\t".'<tr class="' . $row_css_class . '">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names">'.$jcd_metabox['label'].'</th>'."\n";
        	        $output .= "\t\t".'<td><label for="'.esc_attr( $jcd_id ).'"><input type="checkbox" '.$checked.' class="jcd_input_checkbox" value="true"  id="'.esc_attr( $jcd_id ).'" name="'. esc_attr( $jcd_name ) .'" /> ';
        	        $output .= '<span class="jcd_metabox_desc" style="display:inline">'.$jcd_metabox['desc'].'</span></label></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";
        	    }

        	    elseif ( $jcd_metabox['type'] == 'radio' ) {

        	    $array = $jcd_metabox['options'];

        	    if( $array ) {

        	    $output .= "\t".'<tr class="' . $row_css_class . '">';
        	    $output .= "\t\t".'<th class="jcd_metabox_names"><label for="' . esc_attr( $jcd_id ) . '">' . $jcd_metabox['label'] . '</label></th>'."\n";
        	    $output .= "\t\t".'<td>';

        	        foreach ( $array as $id => $option ) {
        	            if($jcd_metaboxvalue == $id) { $checked = ' checked'; } else { $checked=''; }

        	                $output .= '<input type="radio" '.$checked.' value="' . $id . '" class="jcd_input_radio"  name="'. esc_attr( $jcd_name ) .'" />';
        	                $output .= '<span class="jcd_input_radio_desc" style="display:inline">'. $option .'</span><div class="jcd_spacer"></div>';
        	            }
        	            $output .= "\t".'</tr>'."\n";
        	         }
        	    } elseif ( $jcd_metabox['type'] == 'images' ) {

				$i = 0;
				$select_value = '';
				$layout = '';

				foreach ( $jcd_metabox['options'] as $key => $option ) {
					 $i++;

					 $checked = '';
					 $selected = '';
					 if( $jcd_metaboxvalue != '' ) {
					 	if ( $jcd_metaboxvalue == $key ) { $checked = ' checked'; $selected = 'jcd-meta-radio-img-selected'; }
					 }
					 else {
					 	if ($option['std'] == $key) { $checked = ' checked'; }
						elseif ($i == 1) { $checked = ' checked'; $selected = 'jcd-meta-radio-img-selected'; }
						else { $checked=''; }

					 }

						$layout .= '<div class="jcd-meta-radio-img-label">';
						$layout .= '<input type="radio" id="jcd-meta-radio-img-' . $jcd_name . $i . '" class="checkbox jcd-meta-radio-img-radio" value="' . esc_attr($key) . '" name="' . $jcd_name . '" ' . $checked . ' />';
						$layout .= '&nbsp;' . esc_html($key) . '<div class="jcd_spacer"></div></div>';
						$layout .= '<img src="' . esc_url( $option ) . '" alt="" class="jcd-meta-radio-img-img '. $selected .'" onClick="document.getElementById(\'jcd-meta-radio-img-'. esc_js( $jcd_metabox["name"] . $i ) . '\').checked = true;" />';
					}

				$output .= "\t".'<tr class="' . $row_css_class . '">';
				$output .= "\t\t".'<th class="jcd_metabox_names"><label for="' . esc_attr( $jcd_id ) . '">' . $jcd_metabox['label'] . '</label></th>'."\n";
				$output .= "\t\t".'<td class="jcd_metabox_fields">';
				$output .= $layout;
				$output .= '<span class="jcd_metabox_desc">' . $jcd_metabox['desc'] . '</span></td>'."\n";
        	    $output .= "\t".'</tr>'."\n";

				}

        	    elseif( $jcd_metabox['type'] == 'upload' )
        	    {
					if( isset( $jcd_metabox['default'] ) ) $default = $jcd_metabox['default'];
					else $default = '';

        	    	// Add support for the Jcd Media Library-driven Uploader Module // 2010-11-09.
        	    	if ( function_exists( 'jcd_medialibrary_uploader' ) ) {

        	    		$_value = $default;

        	    		$_value = get_post_meta( $post->ID, $jcd_metabox['name'], true );

        	    		$output .= "\t".'<tr class="' . $row_css_class . '">';
	    	            $output .= "\t\t".'<th class="jcd_metabox_names"><label for="'.$jcd_metabox['name'].'">'.$jcd_metabox['label'].'</label></th>'."\n";
	    	            $output .= "\t\t".'<td class="jcd_metabox_fields">'. jcd_medialibrary_uploader( $jcd_metabox['name'], $_value, 'postmeta', $jcd_metabox['desc'], $post->ID );
	    	            $output .= '</td>'."\n";
	    	            $output .= "\t".'</tr>'."\n";

        	    	} else {

	    	            $output .= "\t".'<tr class="' . $row_css_class . '">';
	    	            $output .= "\t\t".'<th class="jcd_metabox_names"><label for="'.esc_attr( $jcd_id ).'">'.$jcd_metabox['label'].'</label></th>'."\n";
	    	            $output .= "\t\t".'<td class="jcd_metabox_fields">'. jcd_uploader_custom_fields( $post->ID, $jcd_name, $default, $jcd_metabox['desc'] );
	    	            $output .= '</td>'."\n";
	    	            $output .= "\t".'</tr>'."\n";

        	        }
        	    }

        	    // Timestamp field.
        	    elseif ( $jcd_metabox['type'] == 'timestamp' ) {
        	    	$jcd_metaboxvalue = get_post_meta($post->ID,$jcd_name,true);

					// Default to current UNIX timestamp.
					if ( $jcd_metaboxvalue == '' ) {
						$jcd_metaboxvalue = time();
					}

        	        $output .= "\t".'<tr class="' . $row_css_class . '">';
        	        $output .= "\t\t".'<th class="jcd_metabox_names"><label for="'.$jcd_metabox.'">'.$jcd_metabox['label'].'</label></th>'."\n";
        	        $output .= "\t\t".'<td><input class="jcd_input_calendar" type="text" name="'.$jcd_name.'[date]" id="'.esc_attr( $jcd_id ).'" value="' . esc_attr( date( 'm/d/Y', $jcd_metaboxvalue ) ) . '">';
        	        $output .= "\t\t" . '<input type="hidden" name="datepicker-image" value="' . get_template_directory_uri() . '/functions/images/calendar.gif" /><br />';

        	        $output .= '<select name="' . $jcd_name . '[hour]" class="jcd-select-timestamp">' . "\n";
						for ( $i = 0; $i <= 23; $i++ ) {

							$j = $i;
							if ( $i < 10 ) {
								$j = '0' . $i;
							}

							$output .= '<option value="' . $i . '"' . selected( date( 'H', $jcd_metaboxvalue ), $j, false ) . '>' . $j . '</option>' . "\n";
						}
					$output .= '</select>' . "\n";

					$output .= '<select name="' . $jcd_name . '[minute]" class="jcd-select-timestamp">' . "\n";
						for ( $i = 0; $i <= 59; $i++ ) {

							$j = $i;
							if ( $i < 10 ) {
								$j = '0' . $i;
							}

							$output .= '<option value="' . $i . '"' . selected( date( 'i', $jcd_metaboxvalue ), $j, false ) .'>' . $j . '</option>' . "\n";
						}
					$output .= '</select>' . "\n";

					$output .= '<select name="' . $jcd_name . '[second]" class="jcd-select-timestamp">' . "\n";
						for ( $i = 0; $i <= 59; $i++ ) {

							$j = $i;
							if ( $i < 10 ) {
								$j = '0' . $i;
							}

							$output .= '<option value="' . $i . '"' . selected( date( 's', $jcd_metaboxvalue ), $j, false ) . '>' . $j . '</option>' . "\n";
						}
					$output .= '</select>' . "\n";

        	        $output .= '<span class="jcd_metabox_desc">'.$jcd_metabox['desc'].'</span></td>'."\n";
        	        $output .= "\t".'</tr>'."\n";

        	    }
        } // End IF Statement
    }

    $output .= '</table>'."\n\n";

    echo $output;
} // End jcd_metabox_create()

/*-----------------------------------------------------------------------------------*/

/**
 * jcd_metabox_handle function.
 *
 * @access public
 * @return void
 */
function jcd_metabox_handle() {

    $pID = '';
    global $globals, $post;

    $jcd_metaboxes = get_option( 'jcd_custom_template' );

    // Sanitize post ID.
    if( isset( $_POST['post_ID'] ) ) {

		$pID = intval( $_POST['post_ID'] );

    } // End IF Statement

    // Don't continue if we don't have a valid post ID.
    if ( $pID == 0 ) {

    	return;

    } // End IF Statement

    $upload_tracking = array();

    if ( isset( $_POST['action'] ) && $_POST['action'] == 'editpost' ) {

        if( is_array( $jcd_metaboxes ) && count( $jcd_metaboxes ) > 0 ) {
            foreach ( $jcd_metaboxes as $k => $jcd_metabox ) { // On Save.. this gets looped in the header response and saves the values submitted
                if( isset( $jcd_metabox['type'] ) && ( in_array( $jcd_metabox['type'], jcd_metabox_fieldtypes() ) ) ) {
    				$var = $jcd_metabox['name'];

    				// Get the current value for checking in the script.
    			    $current_value = '';
    			    $current_value = get_post_meta( $pID, $var, true );

    				if ( isset( $_POST[$var] ) ) {

    					// Sanitize the input.
    					$posted_value = '';
    					$posted_value = $_POST[$var];

    					 // If it doesn't exist, add the post meta.
    					if(get_post_meta( $pID, $var ) == "") {

    						add_post_meta( $pID, $var, $posted_value, true );

    					}
    					// Otherwise, if it's different, update the post meta.
    					elseif( $posted_value != get_post_meta( $pID, $var, true ) ) {

    						update_post_meta( $pID, $var, $posted_value );

    					}
    					// Otherwise, if no value is set, delete the post meta.
    					elseif($posted_value == "") {

    						delete_post_meta( $pID, $var, get_post_meta( $pID, $var, true ) );

    					} // End IF Statement

    				} elseif ( ! isset( $_POST[$var] ) && $jcd_metabox['type'] == 'checkbox' ) {

    					update_post_meta( $pID, $var, 'false' );

    				} else {

    					delete_post_meta( $pID, $var, $current_value ); // Deletes check boxes OR no $_POST

    				} // End IF Statement

                } else if ( $jcd_metabox['type'] == 'timestamp' ) {
                	// Timestamp save logic.

                	// It is assumed that the data comes back in the following format:
    				// date: month/day/year
    				// hour: int(2)
    				// minute: int(2)
    				// second: int(2)

    				$var = $jcd_metabox['name'];

    				// Format the data into a timestamp.
    				$date = $_POST[$var]['date'];

    				$hour = $_POST[$var]['hour'];
    				$minute = $_POST[$var]['minute'];
    				$second = $_POST[$var]['second'];

    				$day = substr( $date, 3, 2 );
    				$month = substr( $date, 0, 2 );
    				$year = substr( $date, 6, 4 );

    				$timestamp = mktime( $hour, $minute, $second, $month, $day, $year );

    				update_post_meta( $pID, $var, $timestamp );

                } elseif( isset( $jcd_metabox['type'] ) && $jcd_metabox['type'] == 'upload' ) { // So, the upload inputs will do this rather

    				$id = $jcd_metabox['name'];
    				$override['action'] = 'editpost';

    			    if(!empty($_FILES['attachement_'.$id]['name'])){ //New upload
    			    $_FILES['attachement_'.$id]['name'] = preg_replace( '/[^a-zA-Z0-9._\-]/', '', $_FILES['attachement_'.$id]['name']);
    			           $uploaded_file = wp_handle_upload($_FILES['attachement_' . $id ],$override);
    			           $uploaded_file['option_name']  = $jcd_metabox['label'];
    			           $upload_tracking[] = $uploaded_file;
    			           update_post_meta( $pID, $id, $uploaded_file['url'] );

    			    } elseif ( empty( $_FILES['attachement_'.$id]['name'] ) && isset( $_POST[ $id ] ) ) {

    			       	// Sanitize the input.
    					$posted_value = '';
    					$posted_value = $_POST[$id];

    			        update_post_meta($pID, $id, $posted_value);

    			    } elseif ( $_POST[ $id ] == '' )  {

    			    	delete_post_meta( $pID, $id, get_post_meta( $pID, $id, true ) );

    			    } // End IF Statement

    			} // End IF Statement

                   // Error Tracking - File upload was not an Image
                   update_option( 'jcd_custom_upload_tracking', $upload_tracking );

                } // End FOREACH Loop

            } // End IF Statement
        }

} // End jcd_metabox_handle()

/*-----------------------------------------------------------------------------------*/

/**
 * jcd_metabox_add function.
 *
 * @access public
 * @since 1.0.0
 * @return void
 */
function jcd_metabox_add() {

    $jcd_metaboxes = get_option( 'jcd_custom_template' );

    if ( function_exists( 'add_meta_box' ) ) {

    	if ( function_exists( 'get_post_types' ) ) {
    		$custom_post_list = get_post_types();

    		// Get the theme name for use in multiple meta boxes.
    		$theme_name = get_option( 'jcd_themename' );

                foreach ($custom_post_list as $type){

                        $settings = array(
                                            'id' => 'jcd-settings',
                                            'title' => $theme_name . __( ' Custom Settings', 'jcd' ),
                                            'callback' => 'jcd_metabox_create',
                                            'page' => $type,
                                            'priority' => 'normal',
                                            'callback_args' => ''
                                    );

                        // Allow child themes/plugins to filter these settings.
                        $settings = apply_filters( 'jcd_metabox_settings', $settings, $type, $settings['id'] );

                        if ( ! empty( $jcd_metaboxes ) ) {
                                add_meta_box( $settings['id'], $settings['title'], $settings['callback'], $settings['page'], $settings['priority'], $settings['callback_args'] );
                        }

                }

    	} else {
    		add_meta_box( 'jcd-settings', $theme_name . ' Custom Settings', 'jcd_metabox_create', 'post', 'normal' );
        	add_meta_box( 'jcd-settings', $theme_name . ' Custom Settings', 'jcd_metabox_create', 'page', 'normal' );
    	}

    }
} // End jcd_metabox_add()

/*-----------------------------------------------------------------------------------*/

/**
 * jcd_metabox_fieldtypes function.
 *
 * @description Return a filterable array of supported field types.
 * @access public
 * @author Matty
 * @return void
 */
function jcd_metabox_fieldtypes() {
	return apply_filters( 'jcd_metabox_fieldtypes', array( 'text', 'calendar', 'time', 'select', 'select2', 'radio', 'checkbox', 'textarea', 'images' ) );
} // End jcd_metabox_fieldtypes()

/*-----------------------------------------------------------------------------------*/

/**
 * jcd_uploader_custom_fields function.
 *
 * @access public
 * @param int $pID
 * @param string $id
 * @param string $std
 * @param string $desc
 * @return void
 */
function jcd_uploader_custom_fields( $pID, $id, $std, $desc ) {

    // Start Uploader
    $upload = get_post_meta( $pID, $id, true);
	$href = cleanSource($upload);
	$uploader = '';
    $uploader .= '<input class="jcd_input_text" name="'.$id.'" type="text" value="'.esc_attr($upload).'" />';
    $uploader .= '<div class="clear"></div>'."\n";
    $uploader .= '<input type="file" name="attachement_'.$id.'" />';
    $uploader .= '<input type="submit" class="button button-highlighted" value="Save" name="save"/>';
    if ( $href )
		$uploader .= '<span class="jcd_metabox_desc">'.$desc.'</span></td>'."\n".'<td class="jcd_metabox_image"><a href="'. $upload .'"><img src="'.get_template_directory_uri().'/functions/thumb.php?src='.$href.'&w=150&h=80&zc=1" alt="" /></a>';

return $uploader;
} // End jcd_uploader_custom_fields()

/*-----------------------------------------------------------------------------------*/

/**
 * jcd_custom_enqueue function.
 *
 * @description Enqueue JavaScript files used with the custom fields.
 * @access public
 * @param string $hook
 * @since 2.6.0
 * @return void
 */
function jcd_custom_enqueue ( $hook ) {
	wp_register_script( 'jquery-ui-datepicker', get_template_directory_uri() . '/functions/js/ui.datepicker.js', array( 'jquery-ui-core' ) );
	wp_register_script( 'jquery-input-mask', get_template_directory_uri() . '/functions/js/jquery.maskedinput-1.2.2.js', array( 'jquery' ) );
	wp_register_script( 'jcd-custom-fields', get_template_directory_uri() . '/functions/js/jcd-custom-fields.js', array( 'jquery' ) );

  	if ( in_array( $hook, array( 'post.php', 'post-new.php', 'page-new.php', 'page.php' ) ) ) {
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'jquery-input-mask' );
  		wp_enqueue_script( 'jcd-custom-fields' );
  	}
} // End jcd_custom_enqueue()

/*-----------------------------------------------------------------------------------*/

/**
 * jcd_custom_enqueue_css function.
 *
 * @description Enqueue CSS files used with the custom fields.
 * @access public
 * @author Matty
 * @since 4.8.0
 * @return void
 */
function jcd_custom_enqueue_css () {
	global $pagenow;

	wp_register_style( 'jcd-custom-fields', get_template_directory_uri() . '/functions/css/jcd-custom-fields.css' );
	wp_register_style( 'jquery-ui-datepicker', get_template_directory_uri() . '/functions/css/jquery-ui-datepicker.css' );

	if ( in_array( $pagenow, array( 'post.php', 'post-new.php', 'page-new.php', 'page.php' ) ) ) {
		wp_enqueue_style( 'jcd-custom-fields' );
		wp_enqueue_style( 'jquery-ui-datepicker' );
	}
} // End jcd_custom_enqueue_css()

/*-----------------------------------------------------------------------------------*/

/**
 * Specify action hooks for the functions above.
 *
 * @access public
 * @since 1.0.0
 * @return void
 */
add_action( 'admin_enqueue_scripts', 'jcd_custom_enqueue', 10, 1 );
add_action( 'admin_print_styles', 'jcd_custom_enqueue_css', 10 );
add_action( 'edit_post', 'jcd_metabox_handle', 10 );
add_action( 'admin_menu', 'jcd_metabox_add', 10 ); // Triggers jcd_metabox_create()
?>
