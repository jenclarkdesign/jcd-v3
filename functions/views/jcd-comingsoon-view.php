<?php
/**
 * Template Name: Coming Soon Page
 * 
 */
global $jcd_options;
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title><?php jcd_title(); ?></title>
		
		<?php wp_head(); ?>
		<?php jcd_head(); ?>
		<?php do_action( 'comingsoon_head' ); ?>
	</head>
	
	
	<?php 
		$use_logo = apply_filters( 'jcd_comingsoon_use_logo', get_option('jcd_use_logo') );
		$logo = apply_filters( 'jcd_comingsoon_logo', get_option('jcd_logo') );
		$launch_date = get_option( 'jcd_comingsoon_date' );
		$additional_text = get_option( 'jcd_comingsoon_text' );
		$mailchimp_list_url = get_option( 'jcd_comingsoon_mailchimp_list_url' );
		$background_image = get_option( 'jcd_comingsoon_background' );
		$header_inverted = get_option( 'jcd_comingsoon_headerbg' ) == 'true' ? 'comingsoon-header-invert' : '';
	?>

	<body class="comingsoon-page <?php echo $header_inverted; ?>">
		<div class="container">

			<div class="comingsoon-wrapper">

				<div class="comingsoon-header">
					<h1><a href="<?php echo home_url('/'); ?>" title="<?php echo get_bloginfo( 'title' ); ?>">
						<?php 
							if( $use_logo == 'true' && $logo != '') {
			                	echo '<img src="'.esc_url( $logo ).'" title="'.get_bloginfo('title').'">'; 
			              	} else {
				              	bloginfo('title');  
				            }
			           	?>
					</a></h1>
					<div class="description"><?php bloginfo('description'); ?></div>
				</div>
				<!-- .comingsoon-header -->
				
				<div class="countdown-wrapper"></div>
				
				<div class="form-wrapper">
					<div class="comingsoon-text">
						<p>
							<?php echo $additional_text; ?><br>

							<?php if( !empty( $mailchimp_list_url ) ) : ?>
								<?php _e("Enter your email address and we'll let you know as soon as it's ready.", 'jcd'); ?>
							<?php endif; ?>
						</p>
					</div>

					<?php if( !empty( $mailchimp_list_url ) ) : ?>
						<!-- Begin MailChimp Signup Form -->
						<form class="comingsoon-subscribe-form" action="<?php echo $mailchimp_list_url ?>" method="post" target="popupwindow" onsubmit="window.open('<?php echo $mailchimp_list_url ?>', 'popupwindow', 'scrollbars=yes,width=650,height=520');return true">
						  <input type="email" class="text-subscribe-email input-text" value="<?php esc_attr_e( 'E-mail', 'jcd' ); ?>" onfocus="if (this.value == '<?php _e( 'E-mail', 'jcd' ); ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e( 'E-mail', 'jcd' ); ?>';}" name="EMAIL" />
						  <input type="submit" class="input-button" value="<?php _e('Subscribe', 'jcd'); ?>" name="subscribe-submit" />              
						</form>
						<!--End mc_embed_signup-->
					<?php endif; ?>
				</div>
				<!-- .form-wrapper -->
				
				<?php
					$comingsoon_extra = apply_filters( 'jcd_comingsoon_extra_content', '' );
					if( $comingsoon_extra ) echo "<div class='comingsoon-extra-content'>{$comingsoon_extra}</div>";
				?>

			</div>
			<!-- .comingsoon-wrapper -->

		</div>
	</body>
	
	<script type="text/javascript">
		(function($){
			$('.countdown-wrapper').countdown({
				until: new Date( '<?php echo $launch_date; ?>' ),
				onExpiry: function() {
					window.location.href = "<?php echo home_url('/'); ?>"
				}
			});

			<?php if( $background_image != '' ) : ?>
				$.anystretch("<?php echo $background_image; ?>");
			<?php endif; ?>

		})(jQuery);
	</script>

	<?php //wp_footer(); ?>
</html>