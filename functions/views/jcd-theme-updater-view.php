<div class="wrap" id="jcd_container">
	<form id="jcd-update-form" method="post">
		<?php wp_nonce_field( 'jcdframework-theme-envato-settings' ); ?>

		<div id="header">
			<div class="logo">
				<img alt="Jcd" src="<?php echo get_template_directory_uri(); ?>/functions/images/logo.png"/>
			</div>
			<div class="theme-info">
				<span class="theme"><?php echo $themename; ?> <?php echo $local_version; ?></span>
				<span class="framework"><?php _e( 'Framework', 'jcd' ); ?> <?php echo $jcd_framework_version; ?></span>
			</div>
			<div class="clear"></div>
		</div><!-- #header -->

		<div id="main">
			<div id="jcd-nav">
				<div id="jcd-nav-shadow"></div><!--/#jcd-nav-shadow-->
				<ul>
					<li class="top-level general current">
						<div class="arrow"><div></div></div>
						<span class="icon"></span>
						<a title="Update Settings" href="#jcd-option-update-settings"><?php _e('Update Settings', 'jcd'); ?></a>
					</li>
					<li class="top-level general">
						<div class="arrow"><div></div></div>
						<span class="icon"></span>
						<a title="Update Theme" href="#jcd-option-update-theme"><?php _e('Update Theme', 'jcd'); ?></a>
					</li>
				</ul>
			</div><!-- #jcd-nav -->

			<div id="content">
				<div class="group" id="jcd-option-update-settings">
					<div class="section">
						<h3 class="heading">User Account Information</h3>
						<div class="option">
							<p>To obtain your API Key, visit your "My Settings" page on any of the Envato Marketplaces. Once a valid connection has been made any changes to the API key below for this username will not effect the results for 5 minutes because they're cached in the database. If you have already made an API connection and just purchase a theme and it's not showing up, wait five minutes and refresh the page.</p>
						</div>
					</div>

					<div class="section">
						<div class="option">
							<div class="option-inner">
								<div class="controls">
									<input type="text" name="jcd_envato_username" id="jcd_envato_username" value="<?php echo $jcd_envato_username; ?>">
								</div>
								<div class="explain"><?php _e('Marketplace Username', 'jcd'); ?></div>
								<div class="clear"></div>
							</div>

							<div class="option-inner">
								<div class="controls">
									<input type="password" name="jcd_envato_api" id="jcd_envato_api" value="<?php echo $jcd_envato_api; ?>">
								</div>
								<div class="explain"><?php _e('Secret API Key', 'jcd'); ?></div>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<div class="section">
						<div class="option save_bar_top" style="padding:0; border:0; background:none;">
							<img style="display:none" src="<?php echo get_template_directory_uri(); ?>/functions/images/loading-bottom.gif" class="ajax-loading-img ajax-loading-img-bottom" alt="Working..." />
							<input type="submit" value="<?php _e('Save Settings', 'colabsthemes'); ?>" class="button submit-button">
						</div>
					</div>
				</div><!-- #jcd-option-update-theme -->

				<div class="group" id="jcd-option-update-theme">
					<div class="section">
						<?php if( $item_detail_html ) : ?>
							<?php echo $item_detail_html; ?>
						<?php else : ?>
							<h2>You are good! This theme is up to date.</h2>
						<?php endif; ?>
					</div>
				</div><!-- #jcd-our-themes -->
				<div class="clear"></div>
			</div>
		</div><!-- #main -->
		<div class="clear"></div>

		<div class="save_bar_top"></div>

	</form>

	<script>
		(function($){
			$(':submit').on('click submit', function(e){
				e.preventDefault();

				var form_data = $('#jcd-update-form').serialize(),
						$ajax_loading = $('.ajax-loading-img'),
						onAnimateOpen = {
							right:0,
							opacity: 'show'
						},
						onAnimateClose = {
							right: -285
						};

				$.ajax({
					type: 'POST',
					url: ajaxurl,
					data: form_data + '&action=save_update_settings',
					beforeSend: function() {
						$ajax_loading.fadeIn();
					},
					success: function( res ) {
						$ajax_loading.fadeOut();
						jQuery.jGrowl(res.messages, {
							theme: res.status,
							animateOpen: onAnimateOpen,
							animateClose: onAnimateClose,
							life: 6000
						});
					}
				});

			});
		})(jQuery);
	</script>
</div><!-- #jcd_container -->
