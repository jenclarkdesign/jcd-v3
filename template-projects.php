<?php
/**
 * Template Name: Projects
 */

	get_header( );
	get_template_part( 'partials/component/page', 'heading' ); 
	
	$projects  = get_all_projects(); ?>
	<div class="main-content-section">
		<div class="wrapper">
			<div class="grid">
				<?php if ( $projects->have_posts() ) : while ( $projects->have_posts() ) : $projects->the_post(); ?>
					<div class="grid__item large--three-twelfths">
						<a href="<?php echo the_permalink(); ?>" <?php post_class('js-mosaic-layout-item grid__item medium--four-twelfths large--three-twelfths one-half project-item'); ?>>
	                        <div class="project-item__inner">
	                            <?php if ( has_post_thumbnail() ) : ?>
	                                <div class="project-category-item__image layer-cover layer-cover-bg" style="background-image: url('<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>')">
	                                </div>
	                            <?php endif; ?>
	                        </div>
	                    </a>
					</div>
				<?php endwhile; endif;
					wp_reset_postdata();?>
			</div>
		</div>
	</div>
		
		
<?php
	get_footer();