<?php get_header();?>

<?php get_template_part( 'partials/component/page', 'heading' ); ?>

<div class="main-content-section wrapper block-section">
	<div class="grid">

		<div class="grid__item large--eight-twelfths main-content">
			<?php while( have_posts() ) : the_post(); ?>
				<article <?php post_class(); ?>>
					<div class="entry-content"><?php the_content(); ?></div>
				</article>
			<?php endwhile; ?>
		</div>
		<div class="grid__item large--four-twelfths">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div><!-- .main-content-section -->

<?php	get_footer(); ?>
