<?php

// Disable error reporting
error_reporting(0);

if ( ! isset( $content_width ) )
	$content_width = 860;

// Set path to Framework and theme specific functions
$functions_path = get_template_directory() . '/functions/';
$includes_path = get_template_directory() . '/includes/';

// Add custom fields support
add_theme_support( 'jcd_advance_custom_fields' );

// Initiate Framework
require_once ($functions_path . 'admin-init.php' );

// Allow ACF to create custom fields for options page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}
/* ===================================================================
  Load the theme-specific files, with support for overriding
  via a child theme.
=================================================================== */

$includes = array(
	'includes/theme-options.php',
	'includes/theme-functions.php',
	'includes/theme-scripts-styles.php',
	'includes/theme-post-types.php',
    'includes/theme-sidebar.php',
	'includes/theme-widgets.php',
	'includes/acf-fields/acf-cf7/acf-cf7.php',
	'includes/wp-instagram/wp-instagram.php',
);


// Allow child themes/plugins to add widgets to be loaded.
$includes = apply_filters( 'jcd_includes', $includes );

// Include all files inside $include variables
foreach ( $includes as $i ) {
	locate_template( $i, true );
}
