<?php
/**
 * Template Name: About Page
 */
get_header();

if (have_posts()) : while (have_posts()) : the_post();

    get_template_part( 'partials/component/page', 'heading' ); ?>

    <div class="main-content-section block-section wrapper">
        <div class="page-description text-align-center">
            <?php the_content(); ?>
        </div>
        <?php get_template_part( 'partials/sections/about-details' ); ?>
        <?php get_template_part( 'partials/sections/membership-section' ); ?>
    </div>
    
    <?php get_template_part( 'partials/sections/testimonial-section' ); ?>
        
<?php endwhile; endif;

get_footer();